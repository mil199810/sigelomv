from django.conf.urls.static import static
from django.conf import settings
from .views import HomePageView, HomePage1View
from django.urls import path


urlpatterns = ([
    #Aqui utilizamos nuestras CBV
    path('', HomePageView.as_view(), name="home"),
    path('prueba/mapa', HomePage1View.as_view())
], 'core')
