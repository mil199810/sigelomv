from django.shortcuts import render, redirect

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages

from django.views.generic.base import TemplateView
from negocios.models import Negocio
from profiles.models import Perfil
from cal.models import Servicio, CategoriaServicio, TiendaServicio

import json


class LoginRequiredMixin(object):
    # esto es un mixxin
    """
    Este mixin requerira que el usuario sea miembro del satff
    """
    # coneste decorator nos aseguramos q no puedan crear contenido sino son staf
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(StaffRequiredMixin, self).dispatch(request, *args, **kwargs)



@method_decorator(login_required, name='dispatch')
class HomePageView(TemplateView):
    template_name = "core/home.html"
    # Se redefine el metodo get para devolver el contexto que queramos
    # los *args, **kwars son por buena practica

    def get(self, request, *args, **kwargs):


        perfil = Perfil.objects.filter(id_user=request.user).exists()        
        # falso o true
        # si existe, es porque hay un perfil asociado
        # sino debe obligar a irse a crearlo
        if perfil == False:
            return redirect('profiles:crear_perfil')
        context = {}
        negocios = Negocio.objects.all()
        servicios = TiendaServicio.objects.all()
        print(servicios)
        negociosjson = construirjson(negocios)  # se invoca la funcion que retorna el json
        serviciosjson1 = serviciosjson(servicios)        
        negociosjson = json.dumps(negociosjson)
        serviciosjson1 = json.dumps(serviciosjson1)
        print(serviciosjson1)
        if len(negociosjson) <= 2:
            negociosjson = 1
        context['data'] = negociosjson
        context['servicios'] = serviciosjson1        
        return render(request, template_name=self.template_name, context=context)


def serviciosjson(query):
    """
    Esta funcion se encaga de retornar el json necesario para que las corrdenadas se muestren en
    el mapa central de la pagina
    """
    dic3 = {}
    lista = []
    for i in query:
        dic0 = {}
        dicq = {}
        dic2 = {}
        lista2 = []
        lista2.insert(0, i.lon)
        lista2.insert(1, i.lat)
        dicq['coordinates'] = lista2
        dic0['geometry'] = dicq
        dic2['title'] = i.nombre
        dic2['description'] = "<center><h6>" + i.direccion + "</h6></center>" + "<br><a class='btn btn-secondary btn-block' href='/servicios/detalles/"+str(i.pk)+"'>Ver Servicio</a> "+\
            '<br><a target="_blank" href="https://api.whatsapp.com/send?phone=57' + i.telefono + \
            '&text=Hola%21%2c%20cómo%20 estas%3f%20Te%20 encontré%20en%20sigelo.org%20y%20quiero%20pedirte%20un%20Servicio&source=&data=&app_absent=" class="btn btn-success btn-icon-split btn-sm">' +\
            '<span class="icon text-white-50">' +\
            '<i class="fab fa-whatsapp fa-2x"></i>' +\
            '</span>' +\
            '<span class="text small">Envíanos un mensaje por WhatsApp</span>' +\
            '</a>'
        dic0['properties'] = dic2
        lista.append(dic0)
        dic3['features'] = lista

    return dic3


def construirjson(query):
    """
    Esta funcion se encaga de retornar el json necesario para que las corrdenadas se muestren en
    el mapa central de la pagina
    """
    dic3 = {}
    lista = []

    for i in query:
        dic0 = {}
        dicq = {}
        dic2 = {}

        lista2 = []
        lista2.insert(0, i.lon)
        lista2.insert(1, i.lat)
        dicq['coordinates'] = lista2
        dic0['geometry'] = dicq
        if i.costo == '0':
            costo = 'Gratis'
        else:
            costo = i.costo

        dic2['title'] = i.nombre
        dic2['description'] = '<div class="h5 mb-2 font-weight-bold text-gray-800">' + i.direccion + '</div>' +\
                "<center>Domicilio<h6>" + costo + "</h6></center>" +\
                    '<a href="negocios/' + str(i.id) + \
            '/store_detail/" class="btn btn-secondary btn-icon-split btn-sm">' +\
            '<span class="icon text-white-50">' +\
            '<i class="fa fa-home fa-2x"></i>' +\
            '</span>' +\
            '<span class="text small">Visita nuestra tienda y conoce nuestros productos</span>' +\
            '</a>' +\
            '<p></p><a target="_blank" href="https://api.whatsapp.com/send?phone=57' + i.telefono + \
            '&text=Hola%21%2c%20cómo%20 estas%3f%20Te%20 encontré%20en%20sigelo.org%20y%20quiero%20pedirte%20un%20Domicilio&source=&data=&app_absent=" class="btn btn-success btn-icon-split btn-sm">' +\
            '<span class="icon text-white-50">' +\
            '<i class="fab fa-whatsapp fa-2x"></i>' +\
            '</span>' +\
            '<span class="text small">Envíanos un mensaje por WhatsApp</span>' +\
            '</a>' +\
            '</div>'
        dic0['properties'] = dic2
        lista.append(dic0)
        dic3['features'] = lista

    return dic3


class HomePage1View(TemplateView):
    template_name = "core/map.html"
    # Se redefine el metodo get para devolver el contexto que queramos
    # los *args, **kwars son por buena practica
