from django.shortcuts import render, redirect
from django.views.generic import View, CreateView, UpdateView
from django.urls import reverse_lazy
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.models import User
from .models import Perfil, Municipio
from .forms import PerfilForm
from django.core import serializers
from django.contrib import messages


# Create your views here.


class CrearPerfil(CreateView):
    model = Perfil
    form_class = PerfilForm
    template_name = 'profiles/crear_perfil.html'
    success_url = reverse_lazy('profiles:Perfillist')


    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        if 'form' not in kwargs:
            kwargs['form'] = self.get_form()
        municipios = Municipio.objects.all()
        kwargs['municipios'] = serializers.serialize("json", municipios)
        return super().get_context_data(**kwargs)
        

    def post(self, request, *args, **kwargs):
        contexto = {}        
        form = self.get_form()
        if form.is_valid():
            perfil = form.save(commit=False)
            perfil.id_user = request.user
            perfil.save()
            return redirect('profiles:Perfillist')
        else:
            print("esto esta malo")
            return self.form_invalid(form)


class PerfilUpdate(UpdateView):
    model = Perfil
    template_name = 'profiles/update_perfil.html'
    form_class = PerfilForm
    success_url = reverse_lazy('profiles:Perfillist')

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        if 'form' not in kwargs:
            kwargs['form'] = self.get_form()
        municipios = Municipio.objects.all()
        kwargs['municipios'] = serializers.serialize("json", municipios)
        return super().get_context_data(**kwargs)


class PerfilList(View):
    model = Perfil

    def get(self, request, *args, **kwargs):
        context = {}
        if request.user.is_authenticated:
            pk = request.user.pk
        else:
            return redirect('account_login')
        perfil = Perfil.objects.get(id_user=pk)
        if not perfil:
            return redirect('account_login')
        context['perfil'] = perfil        
        perfil = Perfil.objects.get(id_user=pk)
        #contexto['contact_detaill'] = contact_detail
        if perfil:
             # si existe un registro de perfil, sino se da la opcion  de crear uno
            id = Perfil.objects.get(id_user=pk)
            context['id'] = id        
        return render(request, template_name='profiles/perfiles_home.html', context=context)


class ViewPerfil(View):
    model = Perfil 
    template_name = 'profiles/view_perfil.html'       

    def get(self, request, *args, **kwargs):
        print('entrando ')
        contexto = {}
        id = self.kwargs['id']
        perfil = Perfil.objects.get(id=id)
        
               
        if perfil:
            contexto['perfil'] = perfil
            contexto['url'] = request.build_absolute_uri()
            if (perfil.vulnerabilidad >= 0.0 and perfil.vulnerabilidad <20.0):
                contexto['color1'] = 'progress-bar bg-secondary'
                contexto['porcentaje1'] = int(perfil.vulnerabilidad)
            elif (perfil.vulnerabilidad >= 20.0 and perfil.vulnerabilidad <40.0):
                contexto['color1'] = 'progress-bar bg-info'
                contexto['porcentaje1'] = int(perfil.vulnerabilidad)
            elif (perfil.vulnerabilidad >= 40.0 and perfil.vulnerabilidad <60.0):
                contexto['color1'] = 'progress-bar bg-danger'
                contexto['porcentaje1'] = int(perfil.vulnerabilidad)
            elif (perfil.vulnerabilidad >= 60.0 and perfil.vulnerabilidad <80.0):
                contexto['color1'] = 'progress-bar bg-warning'
                contexto['porcentaje1'] = int(perfil.vulnerabilidad)
            elif (perfil.vulnerabilidad >= 80.0):
                contexto['color1'] = 'progress-bar bg-primary'
                contexto['porcentaje1'] = int(perfil.vulnerabilidad)


            if (perfil.accesibilidad >= 0.0 and perfil.accesibilidad <20.0):
                contexto['color2'] = 'progress-bar bg-secondary'
                contexto['porcentaje2'] = int(perfil.accesibilidad)
            elif (perfil.accesibilidad >= 20.0 and perfil.accesibilidad <40.0):
                contexto['color2'] = 'progress-bar bg-info'
                contexto['porcentaje2'] = int(perfil.accesibilidad)
            elif (perfil.accesibilidad >= 40.0 and perfil.accesibilidad <60.0):
                contexto['color2'] = 'progress-bar bg-danger'
                contexto['porcentaje2'] = int(perfil.accesibilidad)
            elif (perfil.accesibilidad >= 60.0 and perfil.accesibilidad <80.0):
                contexto['color2'] = 'progress-bar bg-warning'
                contexto['porcentaje2'] = int(perfil.accesibilidad)
            elif (perfil.accesibilidad >= 80.0):
                contexto['color2'] = 'progress-bar bg-primary'
                contexto['porcentaje2'] = int(perfil.accesibilidad)
        else:
            contexto['perfil'] = None
        return render(request, self.template_name, context=contexto)


def cambioTipoUser(request):
    option = request.POST.get('option', '0')
    if request.method == 'POST':
        if option == '1':
            perfil = Perfil.objects.get(id_user=request.user.pk)
            perfil.ingresos = True
            perfil.save()
            return redirect('profiles:Perfillist')
        else:
            return redirect('profiles:Perfillist')
    else:
        print('no post')
        return redirect('profiles:Perfillist')
        
