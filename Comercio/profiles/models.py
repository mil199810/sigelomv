
from django.db import models

# Create your models here.


from django.contrib.auth.models import User

# Create your models here.



class Departamento(models.Model):
    id = models.IntegerField('id')
    codigo_departamento = models.IntegerField(primary_key=True)
    nombre_departamento = models.TextField('nombre_departamento')

    def __str__(self):
        return str(self.nombre_departamento) 


class Municipio(models.Model):
    id = models.IntegerField('id')
    codigo_departamento = models.ForeignKey(Departamento, on_delete = models.CASCADE, blank=True, null=True)
    codigo_dpto_mpio = models.IntegerField(primary_key=True)
    nombre_municipio = models.TextField('nombre_municipio')
    lon = models.FloatField('longitud')
    lat = models.FloatField('latitud')

    def __str__(self):
        return str(self.nombre_municipio)  






TIPO_DNI = [
    ('CC', 'Cedula de Ciudadania'),
    ('CE', 'Cedula de Extranjeria'),
    ('PTE', 'Pasaporte')
]


class Perfil(models.Model):
    id = models.AutoField('id_perfil', primary_key=True)
    nombres = models.CharField('Nombres', max_length=75)
    apellidos = models.CharField('Apellidos', max_length=75)
    telefono = models.CharField('Número Telefónico', max_length=15)
    tipo_dni = models.CharField('Tipo de Documento', max_length=5, choices=TIPO_DNI)
    dni = models.CharField('Número de Documento', max_length=15, default="")    
    fecha_nacimiento = models.DateField('Fecha de Nacimiento', blank=True, null=True)
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)
    ciudad = models.ForeignKey(Municipio, on_delete=models.CASCADE)
    direccion = models.CharField('Dirección', max_length=75)
    lon = models.FloatField('')
    lat = models.FloatField('')
    ingresos = models.BooleanField('¿Genera Ingresos?', default=False)
    id_user = models.OneToOneField(User, on_delete=models.CASCADE)    
    vulnerabilidad = models.FloatField('Vulnerabilidad', default=0.0)
    accesibilidad = models.FloatField('Accesabilidad', default=0.0)


    class Meta:
        verbose_name = 'Perfil'
        verbose_name_plural = 'Perfiles'
        ordering = ['nombres']

    def __str__(self):
        return str(self.id)


 




