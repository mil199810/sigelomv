from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls import url
from django.urls import path, include, re_path
from .views import PerfilList, CrearPerfil, PerfilUpdate, ViewPerfil, cambioTipoUser

registro_perfil = ([
    path('perfil/', PerfilList.as_view(), name="Perfillist"),
    path('perfil/crear/', CrearPerfil.as_view(), name='crear_perfil'),
    path('perfil/editar/<int:pk>', PerfilUpdate.as_view(), name='editar_perfil'),
    path('perfil/detalles/<int:id>', ViewPerfil.as_view(), name='perfil_detalles'),
    path('perfil/change/user/', cambioTipoUser, name='changeUser')
    
], 'profiles')