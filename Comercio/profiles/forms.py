from django import forms
from .models import Perfil
from datetime import date





class PerfilForm(forms.ModelForm):

    def clean(self):        
        super(PerfilForm, self).clean()          
        fecha_nacimiento = self.cleaned_data.get('fecha_nacimiento','')        
        fecha_actual = date.today()
        resultado = fecha_actual.year - fecha_nacimiento.year
        resultado -= ((fecha_actual.month, fecha_actual.day) < (fecha_nacimiento.month, fecha_nacimiento.day))        
        if resultado < 18:             
            raise forms.ValidationError("La edad minima para registrarse es de 18 años")



    class Meta:        
        model = Perfil
        fields = ('nombres', 'apellidos', 'telefono', 'tipo_dni',
                  'dni', 'fecha_nacimiento', 'departamento', 'ciudad', 'direccion', 'lon', 'lat'
                  )
        label = {
            'nombres': 'Nombres',
            'apellidos': 'Apellidos',
            'telefono': 'Numero telefonico',
            'tipo_dni': 'Tipo de documento',
            'dni': 'Numero de identificacion',            
            'fecha_nacimiento': 'Fecha de nacimiento',            
            'departamento': 'Departamento',
            'ciudad': 'Ciudad',
            'direccion': 'Direccion de residencia',
            'lon': '',
            'lat': '' 
        }

        widgets = {
            'nombres': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Nombres'
                }
            ),
            'apellidos': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Apellidos'
                }
            ),
            'telefono': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Número Telefónico'
                }
            ),
            'tipo_dni': forms.Select(
                attrs={
                    'class': 'form-control'
                }
            ),
            'dni': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'fecha_nacimiento': forms.DateInput(
                attrs={
                    'class': 'form-control'                    
                }# eliminar el formato de representacion de fecha definido por el widget                
            ),
            'departamento': forms.Select(
                attrs={
                    'class': 'select2dp form-control'
                }
            ),
            'ciudad': forms.Select(
                attrs={
                    'class': 'select2mp form-control'
                }
            ),
            'direccion': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'lon': forms.NumberInput(
                attrs={
                    'hidden': True,
                    'required': True                    
                }
            ),
            'lat': forms.NumberInput(
                attrs={                    
                    'hidden': True,
                    'required': True
                }
            )
        }
