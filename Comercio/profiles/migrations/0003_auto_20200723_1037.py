# Generated by Django 3.0.6 on 2020-07-23 15:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0002_auto_20200721_2052'),
    ]

    operations = [
        migrations.AlterField(
            model_name='perfil',
            name='direccion',
            field=models.CharField(max_length=75, verbose_name='Dirección'),
        ),
        migrations.AlterField(
            model_name='perfil',
            name='dni',
            field=models.CharField(default='', max_length=15, verbose_name='Número de Documento'),
        ),
        migrations.AlterField(
            model_name='perfil',
            name='ingresos',
            field=models.BooleanField(default=False, verbose_name='¿Genera Ingresos?'),
        ),
        migrations.AlterField(
            model_name='perfil',
            name='telefono',
            field=models.CharField(max_length=15, verbose_name='Número Telefónico'),
        ),
        migrations.AlterField(
            model_name='perfil',
            name='tipo_dni',
            field=models.CharField(choices=[('CC', 'Cedula de Ciudadania'), ('CE', 'Cedula de Extranjeria'), ('PTE', 'Pasaporte')], max_length=5, verbose_name='Tipo de Documento'),
        ),
    ]
