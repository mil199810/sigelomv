from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from import_export import resources
from .models import Departamento, Municipio, Perfil


# Register your models here.




class DepartamentoAdmin(ImportExportModelAdmin):
    pass


class MunicipioAdmin(ImportExportModelAdmin):
    pass


admin.site.register(Perfil)
admin.site.register(Departamento, DepartamentoAdmin)
admin.site.register(Municipio, MunicipioAdmin)
