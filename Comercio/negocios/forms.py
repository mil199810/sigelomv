from django import forms
from .models import Negocio, Producto, CategoriaNegocios


class ProductoForm(forms.ModelForm):

    class Meta:
        model = Producto
        fields = ('nombre', 'foto', 'precio')
        label = {
            'nombre': 'Nombre del Artículo',
            'foto': 'Foto del articulo',
            'precio': 'Precio del articulo',                        
        }
        widgets = {
            'nombre': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Nombre del Artículo'
                }
            ),
            'foto': forms.ClearableFileInput(
                attrs={
                    'class': 'form-control-file mt-3'
                }
            ),
            'precio': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Precio del Artículo'
                }
            )            
        }

    


class NegocioForm(forms.ModelForm):

    class Meta:
        model = Negocio
        fields = ('nombre', 'fachada', 'direccion', 'telefono', 'email_negocio', 'domicilio', 'costo',
                  'categoria', 'descripcion', 'lon', 'lat')
        label = {
            'nombre': 'Nombre del Establecimiento',
            'fachada': 'Foto de la Fachada',
            'direccion': 'Dirección',
            'telefono': 'Telefono del Establecimiento',
            'domicilio': '¿Cuenta con servicio de domicilio?',
            'costo': 'Costo del Domicilio',
            'email_negocio': 'Email del Negocio',
            'categoria': 'Categoría del Establecimiento',
            'descripcion': 'Descripción',
            'lon': '',
            'lat': ''
        }

        widgets = {
            'nombre': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Nombre del Negocio'
                }
            ),
            'fachada': forms.ClearableFileInput(
                attrs={
                    'class': 'form-control-file mt-3'
                }
            ),
            'direccion': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Dirección del Establecimiento'
                }
            ),
            'telefono': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Telefono del Establecimiento'
                }
            ),
            'domicilio': forms.CheckboxInput(
                attrs={
                    'class': 'form-check'
                }
            ),
            'costo': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Costo del Domicilio',
                    'hidden': True
                }
            ),
            'email_negocio': forms.EmailInput(
                attrs={
                    'class': 'form-control form-control-user',
                    'placeholder': 'Correo Electrónico del Negocio'
                }
            ),
            'categoria': forms.Select(
                attrs={                
                    'class': 'form-control'                
                }
            ),
            'descripcion': forms.Textarea(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Descripción de su Establecimiento'
                }
            ),
            'lon': forms.NumberInput(
                attrs={
                    'hidden': True,
                    'required': True
                }
            ),
            'lat': forms.NumberInput(
                attrs={
                    'hidden': True,
                    'required': True
                }
            )
        }
