from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import CreateView, UpdateView, ListView, View, DetailView, DeleteView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.db.models import Q
from django.db.models import Sum
from django.urls import reverse_lazy
from profiles.models import Perfil
from .models import Negocio, Producto, CalificacionNegocio, Order, OrderItem, ShippingOfertante
from cal.models import CategoriaServicio, Servicio, TiendaServicio
from .forms import NegocioForm, ProductoForm
from .utils import cookieCart, cartData, guestOrder
from django.http import JsonResponse
import json
import datetime
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
# Create your views here.




def store(request):
	data = cartData(request)
	cartItems = data['cartItems']
	order = data['order']
	items = data['items']
	products = Producto.objects.all()
	context = {'products':products, 'cartItems':cartItems}
	return render(request, 'negocios/store.html', context)



def store_detail(request,pk):
    #context = {}
    data = cartData(request)
    cartItems = data['cartItems']
    order = data['order']
    items = data['items']
    #print('Action:', pk)
    #negocio = Negocio.objects.filter(id=pk)
    context = {}
    negocio = Negocio.objects.filter(id=pk).first()
    context['negocio'] = negocio
    products = Producto.objects.filter(negocio=negocio)
    cantidad = CalificacionNegocio.objects.filter(negocio=negocio).count()
    suma = CalificacionNegocio.objects.filter(negocio=negocio).aggregate(Sum('puntuacion'))
    context['products'] = products    
    context['cartItems'] = cartItems
    context['pk'] = pk
    if cantidad == 0:
        context['color'] = 'progress-bar bg-info'
        context['porcentaje'] = 60
    else: 
        div = suma.get('puntuacion__sum') / cantidad

        if (div >= 0.0 and div <40.0):
            context['color'] = 'progress-bar bg-info'
            context['porcentaje'] = int(div)        
        elif (div >= 40.0 and div <60.0):
            context['color'] = 'progress-bar bg-warning'
            context['porcentaje'] = int(div)
        elif (div >= 60.0 and div <80.0):
            context['color'] = 'progress-bar bg-info'
            context['porcentaje'] = int(div)
        elif (div >= 80.0):
            context['color'] = 'progress-bar bg-success'
            context['porcentaje'] = int(div)
    #context = {'products':products, 'cartItems':cartItems}
    return render(request, 'negocios/store_detail.html', context)


def cart(request,pk):
    data = cartData(request)
    cartItems = data['cartItems']
    order = data['order']
    items = data['items']
    print(type(cartItems))
    if cartItems >= 1:
        boton = 1
    else:
        boton = 0
    context = {'items':items, 'order':order, 'cartItems':cartItems, 'pk':pk, 'boton':boton}
    return render(request, 'negocios/cart.html', context)


def checkout(request,pk):
    data = cartData(request)
    cartItems = data['cartItems']
    order = data['order']
    items = data['items']
    if cartItems >= 1:
        boton = 1
    else:
        boton = 0
    context = {'items':items, 'order':order, 'cartItems':cartItems, 'pk':pk, 'boton':boton}
    return render(request, 'negocios/checkout.html', context)


def updateItem(request,pk):
	data = json.loads(request.body)
	productId = data['productId']
	action = data['action']
	user = request.user
	producto = Producto.objects.get(id=productId)
	order, created = Order.objects.get_or_create(user=user, complete=False)
	orderItem, created = OrderItem.objects.get_or_create(order=order, producto=producto)
	if action == 'add':
		orderItem.quantity = (orderItem.quantity + 1)
	elif action == 'remove':
		orderItem.quantity = (orderItem.quantity - 1)
	orderItem.save()
	if orderItem.quantity <= 0:
		orderItem.delete()
	return JsonResponse('Item was added', safe=False)


def processOrder(request,pk):
    transaction_id = datetime.datetime.now().timestamp()
    data = json.loads(request.body)
    total = float(data['form1']['total'])
    if request.user.is_authenticated and total > 0:
        user = request.user
        order, created = Order.objects.get_or_create(user=user, complete=False)
    order.transaction_id = transaction_id
    domicilio = order.get_cart_domicilio    
    if total == order.get_cart_total:
        order.complete = True
        order.address=data['shipping']['address']
        order.confirma_cliente=False
    order.save()
    #print(data)
    #print(data)
    context = {}
    items = OrderItem.objects.filter(order=order.pk)
    host = request.get_host()
    context['host'] = host
    context['pk_order'] = order.pk
    context['items'] = items
    context['total'] = total
    context['domicilio'] = domicilio
    template = get_template('negocios/correo_cliente.html')
    content = template.render(context)
    email = EmailMultiAlternatives(
        'Confirmación de orden de compra SIGELO',
        'SIGELO APP',
        settings.EMAIL_HOST_USER,
        [user.email]
    )
    email.attach_alternative(content, 'text/html')
    email.send()
    return JsonResponse('Payment submitted..', safe=False)


def EnviarCorreoOfertante(user,productos,domicilio,pk,host,pk_order):
    total = sum([item.producto.precio*item.quantity for item in productos])
    context = {}
    context['host'] = host
    context['pk_order'] = pk_order
    context['pk'] = pk
    context['items'] = productos
    context['total'] = total+int(domicilio)
    context['domicilio'] = domicilio
    template = get_template('negocios/correo_negocio.html')
    content = template.render(context)
    email = EmailMultiAlternatives(
        'Nueva orden de pedido - SIGELO',
        'SIGELO APP',
        settings.EMAIL_HOST_USER,
        [user]
    )
    email.attach_alternative(content, 'text/html')
    email.send()



def ConfirmarOrden(request,pk_order,opc):
    host = request.get_host()
    if opc == 0:
        print('La orden fue rechazada')
    else:    
        ordershipping = Order.objects.filter(pk=pk_order)
        ordershi = Order.objects.get(pk=pk_order)
        ordershi.confirma_cliente = True
        ordershi.save()
        items = OrderItem.objects.filter(order=pk_order)
        negocios = []
        for item in items:
            if not item.producto.negocio.pk in negocios:
                negocios.append(item.producto.negocio.pk)
                dom=item.producto.negocio.costo
                items2 = OrderItem.objects.filter(order=pk_order, producto__negocio=item.producto.negocio.pk)
                pk = item.producto.negocio.pk
                ShippingOfertante.objects.create(
                    ofertante = item.producto.negocio.perfil.id_user,
                    order=Order.objects.get(pk=pk_order),
                    order_enviada = False,
                    negocio = item.producto.negocio.id
                )
                EnviarCorreoOfertante(item.producto.negocio.perfil.id_user.email,items2,dom,pk,host,pk_order)
    return redirect('core:home')


def EnvioPedido(request,pk,pk_order):
    negocio=Negocio.objects.get(pk=pk)
    shippingofer = ShippingOfertante.objects.get(ofertante=negocio.perfil.id_user.pk,order=pk_order)
    ordershi = Order.objects.get(pk=pk_order)
    shippingofer.order_enviada = True
    shippingofer.fecha_envio = datetime.datetime.now()
    context = {}
    items = OrderItem.objects.filter(order=pk_order,producto__negocio=pk)
    total = sum([item.producto.precio*item.quantity for item in items]) + int(negocio.costo)
    context['fecha'] = shippingofer.fecha_envio
    context['pk_order'] = pk_order
    context['items'] = items
    context['total'] = total
    context['negocio'] = negocio.nombre
    context['domicilio'] = negocio.costo
    template = get_template('negocios/correo_envio.html')
    content = template.render(context)
    email = EmailMultiAlternatives(
        'Su pedido ha sido enviado',
        'SIGELO APP',
        settings.EMAIL_HOST_USER,
        [ordershi.user.email]
    )
    email.attach_alternative(content, 'text/html')
    email.send()
    shippingofer.save()
    pk_user = negocio.perfil.id_user.pk
    return redirect('negocios:ordenes', pk=pk_user)


@login_required
def Ordenes(request,pk):
    context = {}
    current_user = request.user
    items = OrderItem.objects.filter(producto__negocio__perfil__id_user=pk).exists()    
    if items:
        items = OrderItem.objects.filter(producto__negocio__perfil__id_user=pk)        
        confima_clientes = {}
        order_enviadas = {}
        list_productos = {}
        creada= {}
        envio= {}
        total = {}
        env = {}
        negocios = {}
        pk_negocios = {}
        direccion = {}
        id_ordenes = []
        for item in items:
            if not item.order.pk in id_ordenes:
                id_ordenes.append(item.order.pk)
                #shippin = ShippingOfertante.objects.get(ofertante=item.producto.negocio.perfil.id_user.pk,order=item.order.pk)
                orden = Order.objects.get(pk=item.order.pk)
                productos = OrderItem.objects.filter(order = item.order.pk, producto__negocio__perfil__id_user=pk)
                confima_clientes[str(item.order.pk)] = orden.confirma_cliente
                creada[str(item.order.pk)] = orden.created
                direccion[str(item.order.pk)] = orden.address
                list_productos[str(item.order.pk)] = productos
                dom = item.producto.negocio.costo
                negocios[str(item.order.pk)] = item.producto.negocio.nombre 
                pk_negocios[str(item.order.pk)] = item.producto.negocio.pk               
                if ShippingOfertante.objects.filter(order=item.order.pk,ofertante__perfil__id_user=pk).exists():
                    shippin = ShippingOfertante.objects.get(ofertante=item.producto.negocio.perfil.id_user.pk,order=item.order.pk)
                    order_enviadas[str(item.order.pk)] = shippin.order_enviada
                    envio[str(item.order.pk)] = shippin.fecha_envio
                    env = shippin
                total[str(item.order.pk)] = sum([item2.producto.precio*item2.quantity for item2 in productos])+int(dom)
        context = {'direccion':direccion, 'ordenes': list_productos, 'pk_negocios': pk_negocios, 'pk': pk,'domicilio': dom,'total':total , 'confima_cliente':confima_clientes, 'order_enviada':order_enviadas,'creada':creada, 'envio':envio, 'negocios':negocios}       
        if 'delete_queue' in request.POST:
                id = int(request.POST.get("id"))
                current_queue = Order.objects.get(pk=id)
                current_queue.delete()
                return redirect('negocios:ordenes', pk=pk)
        # else:
        return render(request, 'negocios/ordenes.html', context)
    else:
        print('no existe ')
        context['existe'] = False
        context['pk'] = pk
        return render(request, 'negocios/ordenes.html', context)


@login_required
def VerOrden(request,pk,pk_order):
    context = {}    
    current_user = request.user
    items = OrderItem.objects.filter(order = pk_order, producto__negocio=pk).exists()
    #print(items)    
    if items:
        items = OrderItem.objects.filter(order = pk_order, producto__negocio=pk)
        confima_clientes = {}
        order_enviadas = {}
        list_productos = {}
        negocios = {}
        creada= {}
        envio= {}
        total = {}
        env = {}
        direccion = {}
        pk_negocios = {}
        id_ordenes = []
        for item in items:
            if not item.order.pk in id_ordenes:
                id_ordenes.append(item.order.pk)
                orden = Order.objects.get(pk=item.order.pk)
                productos = OrderItem.objects.filter(order = item.order.pk, producto__negocio=pk)
                confima_clientes[str(item.order.pk)] = orden.confirma_cliente
                creada[str(item.order.pk)] = orden.created
                direccion[str(item.order.pk)] = orden.address
                list_productos[str(item.order.pk)] = productos
                dom = item.producto.negocio.costo
                print(type(dom))
                negocios[str(item.order.pk)] = item.producto.negocio.nombre
                pk_negocios[str(item.order.pk)] = item.producto.negocio.pk 
                #print(negocios)
                if ShippingOfertante.objects.filter(order=item.order.pk,ofertante__perfil__id_user=pk).exists():
                    shippin = ShippingOfertante.objects.get(ofertante=item.producto.negocio.perfil.id_user.pk,order=item.order.pk)
                    order_enviadas[str(item.order.pk)] = shippin.order_enviada
                    envio[str(item.order.pk)] = shippin.fecha_envio
                    env = shippin
                total[str(item.order.pk)] = sum([item2.producto.precio*item2.quantity for item2 in productos])+int(dom)    
        context = {'direccion':direccion, 'ordenes': list_productos, 'pk_negocios': pk_negocios, 'pk': pk,'domicilio': dom,'total':total , 'confima_cliente':confima_clientes, 'order_enviada':order_enviadas,'creada':creada, 'envio':envio, 'env':env, 'negocios':negocios}       
        if 'delete_queue' in request.POST:
                id = int(request.POST.get("id"))
                current_queue = Order.objects.get(pk=id)
                current_queue.delete()
                return redirect('negocios:ordenes', pk=pk)
        # else:
        return render(request, 'negocios/ordenes.html', context)
    else:
        context['existe'] = False
        context['pk'] = pk
        return render(request, 'negocios/ordenes.html', context)


@login_required
def Pedidos(request,pk):
    context = {}     
    current_user = request.user
    items = Order.objects.filter(user=pk).exists()    
    if items:
        items = Order.objects.filter(user=pk)        
        confima_clientes = {}
        order_enviadas = {}
        list_productos = {}
        negocios = {}
        creada= {}
        envio= {}
        dom = {}
        total = {}
        env = {}
        id_ordenes = []
        for item in items:
            print(item)
            if not item.pk in id_ordenes and Order.objects.filter(pk = item.pk).exists():
                id_ordenes.append(item.pk)
                # shippin = ShippingAddress.objects.get(order = item.pk)
                # productos = OrderItem.objects.filter(order = item.pk)
                # confima_clientes[str(item.pk)] = shippin.confirma_cliente
                # order_enviadas[str(item.pk)] = shippin.order_enviada
                # creada[str(item.pk)] = shippin.created
                # envio[str(item.pk)] = shippin.fecha_envio
                # list_productos[str(item.pk)] = productos
                # dom[str(item.pk)] = item.get_cart_domicilio
                # id_ordenes.append(item.pk)
                orden = Order.objects.get(pk=item.pk)
                productos = OrderItem.objects.filter(order = item.pk)
                confima_clientes[str(item.pk)] = orden.confirma_cliente
                creada[str(item.pk)] = orden.created
                list_productos[str(item.pk)] = productos
                #negocios[str(item.pk)] = item.get_negocio
                dom[str(item.pk)] = item.get_cart_domicilio
                total[str(item.pk)] = sum([item2.producto.precio*item2.quantity for item2 in productos])+item.get_cart_domicilio
                shippin = ShippingOfertante.objects.filter(order=item.pk)
                env = shippin
        context = {'ordenes': list_productos, 'pk': pk,'domicilio': dom,'total':total , 'confima_cliente':confima_clientes, 'order_enviada':order_enviadas,'creada':creada, 'envio':envio, 'env':env}       
        if 'delete_queue' in request.POST:
                id = int(request.POST.get("id"))
                current_queue = Order.objects.get(pk=id)
                current_queue.delete()
                return redirect('negocios:pedidos', pk=pk)
        # else:        
        return render(request, 'negocios/pedidos.html', context)
    else:
        context['existe'] = False
        return render(request, 'negocios/pedidos.html', context)


class LoginRequiredMixin(object):

    # esto es un mixxin
    """
    Este mixin requerira que el usuario sea miembro del satff
    """
    # coneste decorator nos aseguramos q no puedan crear contenido sino son staf
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(StaffRequiredMixin, self).dispatch(request, *args, **kwargs)


@method_decorator(login_required, name='dispatch')
class CrearNegocio(CreateView):
    model = Negocio
    form_class = NegocioForm
    template_name = 'negocios/form_negocios.html'
    success_url = reverse_lazy('profiles:Perfillist')


    def post(self, request, *args, **kwargs):
        contexto = {}
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            negocio = form.save(commit=False)
            negocio.perfil_id = Perfil.objects.filter(
                id_user=request.user).only('id')
            negocio.save()
            return redirect('core:home')
        else:
            form = self.form_class
            contexto['msj'] = 'Formulario invalido'
        contexto['form'] = form
        return super(CrearNegocio, self).form_invalid(form)

        # return render(request, self.template_name, context=contexto)

class NegocioUpdate(UpdateView):
    model = Negocio
    template_name = 'negocios/update_negocio.html'
    form_class = NegocioForm
    success_url = reverse_lazy('profiles:Perfillist')


@method_decorator(login_required, name='dispatch')
class NegocioListView(ListView):
    model = Negocio
    template_name = 'negocios/list_negocios.html'
    paginate_by = 6
    ordering = ['-updated']

    def get(self, request, *args, **kwargs):
        context = {}
        negocios = Negocio.objects.filter(perfil__id_user=request.user.id)
        context['negocios'] = negocios
        print(negocios)
        return render(request, self.template_name, context=context)


@method_decorator(login_required, name='dispatch')
class CrearProducto(CreateView):
    model = Producto
    form_class = ProductoForm
    template_name = 'negocios/form_producto.html'
    success_url = reverse_lazy('negocios:list_negocios')

    def post(self, request, *args, **kwargs):
        contexto = {}
        negocio = self.kwargs.get('pk_negocio', None)
        print(negocio)
        negocio = Negocio.objects.get(id=negocio)
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            producto = form.save(commit=False)
            producto.user = request.user
            producto.negocio = negocio
            producto.save()
            return redirect('negocios:list_negocios')
        else:
            form = self.form_class
            contexto['msj'] = 'Formulario invalido'
        contexto['form'] = form
        return super(CrearProducto, self).form_invalid(form)

    

@method_decorator(login_required, name='dispatch')
class ProductoUpdate(UpdateView):
    model = Producto
    template_name = 'negocios/update_producto.html'
    form_class = ProductoForm
    success_url = reverse_lazy('negocios:list_negocios')


@method_decorator(login_required, name='dispatch')
class ListProductos(View):
    model = Producto
    template_name = 'negocios/list_productos.html'
    paginate_by = 6
    ordering = ['-updated']

    def get(self, request, *args, **kwargs):
        contexto = {}
        id = self.kwargs['id']
        productos = Producto.objects.filter(negocio=id)
        contexto['id'] = id
        contexto['productos'] = productos
        return render(request, self.template_name, context=contexto)


@method_decorator(login_required, name='dispatch')
class NegocioDetail(DetailView):
    model= Negocio
    template_name = 'negocios/negocio_detail.html'


    def get(self, request, *args, **kwargs):
        context = {}
        negocio = Negocio.objects.filter(id=self.kwargs['pk']).first()
        productos = Producto.objects.filter(negocio=negocio)
        context['negocio'] = negocio
        context['productos'] = productos

        cantidad = CalificacionNegocio.objects.filter(negocio=self.kwargs['pk']).count()
        suma = CalificacionNegocio.objects.filter(negocio=self.kwargs['pk']).aggregate(Sum('puntuacion'))
        print(cantidad, suma)

        if cantidad == 0:
            context['color'] = 'progress-bar bg-info'
            context['porcentaje'] = 60
        else: 
            div = suma.get('puntuacion__sum') / cantidad

            if (div >= 0.0 and div <40.0):
                context['color'] = 'progress-bar bg-info'
                context['porcentaje'] = int(div)        
            elif (div >= 40.0 and div <60.0):
                context['color'] = 'progress-bar bg-warning'
                context['porcentaje'] = int(div)
            elif (div >= 60.0 and div <80.0):
                context['color'] = 'progress-bar bg-info'
                context['porcentaje'] = int(div)
            elif (div >= 80.0):
                context['color'] = 'progress-bar bg-success'
                context['porcentaje'] = int(div)
        return render(request, self.template_name, context=context)


def DeleteNegocio(request):        
    pk = request.GET.get('pk')      
    negocio = Negocio.objects.get(id=pk)
    negocio.delete()
    return redirect('negocios:list_negocios')


def DeleteProducto(request):        
    pk = request.GET.get('pk')      
    producto = Producto.objects.get(id=pk)
    id = producto.negocio.id
    producto.delete()    
    return redirect('negocios:list_producto', id=id)


    """ def get_success_url(self):
        return reverse_lazy('negocios:list_negocios', args=(self.object.id, )) """

def serviciosjson(query):
    """
    Esta funcion se encaga de retornar el json necesario para que las corrdenadas se muestren en
    el mapa central de la pagina
    """
    dic3 = {}
    lista = []
    for i in query:
        dic0 = {}
        dicq = {}
        dic2 = {}
        lista2 = []
        lista2.insert(0, i.lon)
        lista2.insert(1, i.lat)
        dicq['coordinates'] = lista2
        dic0['geometry'] = dicq
        dic2['title'] = i.nombre
        dic2['description'] = "<center><h6>" + i.direccion + "</h6></center>" + "<br><a class='btn btn-secondary btn-block' href='/servicios/detalles/"+str(i.pk)+"'>Ver Servicio</a> "+\
            '<br><a target="_blank" href="https://api.whatsapp.com/send?phone=57' + i.telefono + \
            '&text=Hola%21%2c%20cómo%20 estas%3f%20Te%20 encontré%20en%20sigelo.org%20y%20quiero%20pedirte%20un%20Servicio&source=&data=&app_absent=" class="btn btn-success btn-icon-split btn-sm">' +\
            '<span class="icon text-white-50">' +\
            '<i class="fab fa-whatsapp fa-2x"></i>' +\
            '</span>' +\
            '<span class="text small">Envíanos un mensaje por WhatsApp</span>' +\
            '</a>'
        dic0['properties'] = dic2
        lista.append(dic0)
        dic3['features'] = lista

    return dic3



def construirjson(query):
    """
    Esta funcion se encaga de retornar el json necesario para que las corrdenadas se muestren en
    el mapa central de la pagina
    """
    dic3 = {}
    lista = []

    for i in query:
        dic0 = {}
        dicq = {}
        dic2 = {}

        lista2 = []
        lista2.insert(0, i.lon)
        lista2.insert(1, i.lat)
        dicq['coordinates'] = lista2
        dic0['geometry'] = dicq
        if i.costo == '0':
            costo = 'Gratis'
        else:
            costo = i.costo

        dic2['title'] = i.nombre
        dic2['description'] = '<div class="h5 mb-2 font-weight-bold text-gray-800">' + i.direccion + '</div>' +\
                "<center>Domicilio<h6>" + costo + "</h6></center>" +\
            '<a href="/negocios/' + str(i.id) + \
            '/store_detail/" class="btn btn-secondary btn-icon-split btn-sm">' +\
            '<span class="icon text-white-50">' +\
            '<i class="fa fa-home fa-2x"></i>' +\
            '</span>' +\
            '<span class="text small">Visita nuestra tienda y conoce nuestros productos</span>' +\
            '</a>' +\
            '<p></p><a target="_blank" href="https://api.whatsapp.com/send?phone=57' + i.telefono + \
            '&text=Hola%21%2c%20cómo%20 estas%3f%20Te%20 encontré%20en%20sigelo.org%20y%20quiero%20pedirte%20un%20Domicilio&source=&data=&app_absent=" class="btn btn-success btn-icon-split btn-sm">' +\
            '<span class="icon text-white-50">' +\
            '<i class="fab fa-whatsapp fa-2x"></i>' +\
            '</span>' +\
            '<span class="text small">Envíanos un mensaje por WhatsApp</span>' +\
            '</a>' +\
            '</div>'
        dic0['properties'] = dic2
        lista.append(dic0)
        dic3['features'] = lista

    return dic3

def is_valid_query(query):
    return query != '' and query is not None

@login_required
def search(request):
    print(request.META.get('HTTP_REFERER'))
    contexto = {} 

    query = request.GET.get('q', '')
    categoria = request.GET.get('id_category', '') # variables de la peticon get
    


    if categoria == '1':
        print(categoria)
        # filtra por productos
        contexto['opcion'] = 1      
        if is_valid_query(query):             
            # construir consulta en base a productos        
            qset2 = (            
            Q(nombre__icontains=query) |
            Q(producto__nombre=query) |
            Q(producto__nombre__icontains = query)
            )            
            results = Negocio.objects.filter(qset2)
        else:
            print('query invalido')
            # si el query es invalido entra a filtrar por categoria
            # de producto, siguiendo la relacion del modelo            
            results = Negocio.objects.all()
            # con esta se encutra el negocio q se ve reflejado en el mapa        
         # filtra para la busqueda en los productos        

         # filtro para la busqueda de establecimiento
        results = construirjson(results)
        contexto['negocios'] = results
        
    elif categoria == '2':
        print(categoria)
        # filtra por servicios        
        if is_valid_query(query):       
            # se construye consulta en base a los servicios
            qset = (
                Q(nombre__icontains=query) 
            )
            results = TiendaServicio.objects.filter(qset) 
        else:
            results = TiendaServicio.objects.all()
        print(results)
        results = serviciosjson(results)
        contexto['results'] = results
    else:
        return redirect('core:home')

    contexto['query'] = query
    return render(request, "negocios/search.html", context=contexto)



def calificacion_negocio(request):
    puntos = request.GET.get('estrellas', 1)
    id_negocio = request.GET.get('id_negocio')
    comentario = request.GET.get('comentario')
    print(puntos, id_negocio, request.user.id, comentario)
    CalificacionNegocio.objects.create(puntuacion=puntos, negocio_id=id_negocio, user=request.user, comentario=comentario)
    return redirect('negocios:search')



class CalificacionesNegocio(ListView):
    model = CalificacionNegocio
    template_name = 'negocios/list_calificaciones.html'
    paginate_by = 9
    ordering = ['-updated']


    def get(self, request, *args, **kwargs):
        context = {}
        pk = self.kwargs['pk']
        print(pk)
        comentarios = CalificacionNegocio.objects.filter(negocio__perfil__id_user=pk)
        context['comentarios'] = comentarios
        print(comentarios)
        return render(request, self.template_name, context=context)



def DeleteComentarios(request):
    url_anterior = request.META.get('HTTP_REFERER')
    pk = request.GET.get('option', '')
    if pk != '':
        c = CalificacionNegocio.objects.get(pk=pk)
        c.delete()
        print('eliminada')
        return redirect(url_anterior)
    else:
        return redirect(url_anterior)



def detailNegocioProducto(request, pk_negocio):
    context = {}
    negocio = Negocio.objects.filter(id=pk_negocio).exists()
    if negocio:
        negocio = Negocio.objects.get(id=pk_negocio)
        productos = Producto.objects.filter(negocio__id=negocio.id)
        print(productos)
        print(negocio)
        context['negocio'] = negocio
        context['productos'] = productos
        return render(request, 'negocios/detail_negocios.html', context=context)
    else:
        return redirect('core:home')

