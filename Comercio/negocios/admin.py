from django.contrib import admin
from .models import CategoriaNegocios, Negocio, Producto, CalificacionNegocio, Producto, Order, OrderItem, ShippingOfertante
from import_export.admin import ImportExportModelAdmin
from import_export import resources


class CategoriaNegociosAdmin(ImportExportModelAdmin):
    pass



admin.site.register(Negocio)
admin.site.register(CategoriaNegocios, CategoriaNegociosAdmin)
admin.site.register(Producto)
admin.site.register(CalificacionNegocio)

#admin.site.register(Customer)
#admin.site.register(Producto)
admin.site.register(Order)
admin.site.register(OrderItem)
admin.site.register(ShippingOfertante)
