from django.db import models
from django.contrib.auth.models import User
from profiles.models import Perfil

# Create your models here.


class CategoriaNegocios(models.Model):
    nombre = models.CharField('Nombre', max_length=55)

    class Meta:
        verbose_name = 'Categoria de negocio'
        ordering = ['-nombre']
        verbose_name_plural = 'Categorias de negocios'

    def __str__(self):
        return self.nombre


class Negocio(models.Model):
    id = models.AutoField('id', primary_key=True)
    nombre = models.CharField('Nombre del Establecimiento', max_length=70)
    fachada = models.ImageField(
        'Fachada del Establecimiento', upload_to='fachadas/%Y/%m/%d/')
    direccion = models.CharField('Dirección', max_length=50)
    telefono = models.CharField('Número Telefónico', max_length=10)
    domicilio = models.BooleanField('Servicio de Domicilio')
    costo = models.CharField('', max_length=10, default='0')
    email_negocio = models.EmailField('Correo Electrónico del Negocio', blank=True)
    categoria = models.ForeignKey(CategoriaNegocios, on_delete=models.CASCADE, verbose_name='Categoría')
    descripcion = models.TextField('Descripción')
    lon = models.FloatField('')
    lat = models.FloatField('')
    perfil = models.ForeignKey(Perfil, on_delete=models.CASCADE, verbose_name='Usuario asociado')
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")


    class Meta:
        verbose_name = 'Negocio'
        ordering = ['-created']
        verbose_name_plural ='Negocios'
        unique_together = (('nombre', 'direccion'),)

    def __str__(self):
        return self.nombre


class Producto(models.Model):
    id = models.AutoField('id', primary_key=True)
    nombre = models.CharField('nombre', max_length=55)
    foto = models.ImageField('Imagen del producto', upload_to='productos/%Y/%m/%d/')
    precio = models.IntegerField('precio') 
    negocio = models.ForeignKey(Negocio, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = 'Producto'
        ordering = ['-updated', 'nombre']
        verbose_name_plural = 'Productos'

    def __str__(self):
        return self.nombre




class CalificacionNegocio(models.Model):
    id = models.AutoField('id', primary_key=True)
    comentario = models.TextField('Comentario', blank=True)
    puntuacion = models.IntegerField('Puntuacion')    
    negocio = models.ForeignKey(Negocio, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")


    class Meta:
        verbose_name = 'Calificacion'
        ordering = ['-created', 'puntuacion']
        verbose_name_plural = 'Calificaciones'

    def __str__(self):
        return str(self.puntuacion)


""" class Customer(models.Model):
	user = models.OneToOneField(User, null=True, blank=True, on_delete=models.CASCADE)
	name = models.CharField(max_length=200, null=True)
	email = models.CharField(max_length=200)

	def __str__(self):
		return self.name """


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    date_ordered = models.DateTimeField(auto_now_add=True)
    complete = models.BooleanField(default=False)
    transaction_id = models.CharField(max_length=100, null=True)
    address = models.CharField(max_length=200, null=False)
    confirma_cliente = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    def __str__(self):
        return str(self.id)
        
    @property
    def get_cart_total(self):
        orderitems = self.orderitem_set.all()
        if len(orderitems) >= 1:
            total = sum([item.get_total for item in orderitems])+self.get_cart_domicilio
        else:
            total = 0
        return total 

    @property
    def get_cart_items(self):
        orderitems = self.orderitem_set.all()
        total = sum([item.quantity for item in orderitems])
        return total 


    @property
    def get_cart_domicilio(self):
        orderitems = self.orderitem_set.all()
        negocios = []
        domicilio = 0
        if len(orderitems) >= 1:
            for item in orderitems:
                if not item.producto.negocio.pk in negocios:
                    negocios.append(item.producto.negocio.pk)
                    domicilio = domicilio + int(item.producto.negocio.costo) 
                else:
                    domicilio = 0
        return domicilio 


class OrderItem(models.Model):
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE, null=True)
    order = models.ForeignKey(Order, on_delete=models.CASCADE, null=True)
    quantity = models.IntegerField(default=0, null=True, blank=True)
    date_added = models.DateTimeField(auto_now_add=True)

    @property
    def get_total(self):
        total = self.producto.precio * self.quantity
        return total

    @property
    def get_domicilio(self):
        domicilio = int(self.producto.negocio.costo)
        return domicilio

    @property
    def get_negocio(self):
        negocio = self.producto.negocio.id
        return negocio


class ShippingOfertante(models.Model):
    ofertante = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    order = models.ForeignKey(Order, on_delete=models.CASCADE, null=True)
    order_enviada = models.BooleanField(default=False)      
    fecha_envio = models.DateTimeField(verbose_name="Fecha de envio", blank=True, null = True)
    negocio = models.IntegerField(verbose_name='negocio', blank=True, null=True)


    class Meta:
        verbose_name = 'Shipping ofertante'           
        unique_together = (('ofertante', 'order'),)

