# Generated by Django 3.0.8 on 2020-07-16 15:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('negocios', '0002_delete_categoriaproducto'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='negocio',
            unique_together={('nombre', 'direccion')},
        ),
    ]
