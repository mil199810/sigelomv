# Generated by Django 3.0.6 on 2020-07-16 21:42

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('negocios', '0003_auto_20200716_1505'),
    ]

    operations = [
        migrations.AddField(
            model_name='shippingaddress',
            name='ofertante',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='ofertant', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='shippingaddress',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='demandante', to=settings.AUTH_USER_MODEL),
        ),
    ]
