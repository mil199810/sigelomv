# Generated by Django 3.1 on 2020-08-19 04:27

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('negocios', '0019_auto_20200818_2321'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='shippingofertante',
            unique_together={('ofertante', 'order')},
        ),
        migrations.RemoveField(
            model_name='shippingofertante',
            name='shippingaddress',
        ),
    ]
