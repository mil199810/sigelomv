import json
from django.contrib.auth.models import User
from .models import *

def cookieCart(request):
	#Create empty cart for now for non-logged in user
	try:
		cart = json.loads(request.COOKIES['cart'])
	except:
		cart = {}
	items = []
	order = {'get_cart_total':0, 'get_cart_items':0, 'shipping':False}
	cartItems = order['get_cart_items']

	for i in cart:
		#We use try block to prevent items in cart that may have been removed from causing error
		try:
			cartItems += cart[i]['quantity']
			producto = Producto.objects.get(id=i)
			total = (producto.price * cart[i]['quantity'])
			order['get_cart_total'] += total
			order['get_cart_items'] += cart[i]['quantity']
			item = {
				'id':producto.id,
				'producto':{'id':producto.id,'nombre':producto.nombre, 'precio':producto.precio, 
				'foto':producto.foto}, 'quantity':cart[i]['quantity'],
				'get_total':total,
				}
			items.append(item)
		except:
			pass	
	return {'cartItems':cartItems ,'order':order, 'items':items}



def cartData(request):
	if request.user.is_authenticated:
		user = request.user
		order, created = Order.objects.get_or_create(user=user, complete=False)
		items = order.orderitem_set.all()
		cartItems = order.get_cart_items
	else:
		cookieData = cookieCart(request)
		cartItems = cookieData['cartItems']
		order = cookieData['order']
		items = cookieData['items']
	return {'cartItems':cartItems ,'order':order, 'items':items}


	
def guestOrder(request, data):
	name = data['form']['name']
	email = data['form']['email']

	cookieData = cookieCart(request)
	items = cookieData['items']

	user, created = Customer.objects.get_or_create(
			email=email,
			)
	user.name = name
	user.save()

	order = Order.objects.create(
		user=user,
		complete=False,
		)

	for item in items:
		producto = Producto.objects.get(id=item['id'])
		orderItem = OrderItem.objects.create(
			producto=producto,
			order=order,
			quantity=item['quantity'],
		)
	return user, order

