from django.conf.urls.static import static
from django.conf import settings
from django.urls import path
from .views import CrearNegocio, NegocioUpdate, NegocioListView, NegocioDetail, DeleteNegocio
from .views import CrearProducto, ProductoUpdate, ListProductos, CalificacionesNegocio, DeleteComentarios
from . import views


registro_negocios = ([
    path('', NegocioListView.as_view(), name='list_negocios'),
    path('<int:pk_negocio>/detail/', views.detailNegocioProducto, name='detail_negocio'),
    path('delete/', views.DeleteNegocio, name='delete_negocios'),
    path('<int:pk>/cart/', views.cart, name="cart"),
    path('<int:pk>/checkout/', views.checkout, name="checkout"),
    path('<int:pk>/update_item', views.updateItem, name="update_item"),
    path('<int:pk>/store_detail/', views.store_detail, name='store_detail'),
    path('<int:pk>/process_order', views.processOrder, name="process_order"),
    path('<int:pk_order>/<int:opc>/confirmar_orden', views.ConfirmarOrden, name="confirmar_orden"),
    path('<int:pk>/ordenes/', views.Ordenes, name='ordenes'),
    path('<int:pk_order>/<int:pk>/ver_orden/', views.VerOrden, name='ver_orden'),
    path('<int:pk_order>/<int:pk>/', views.EnvioPedido, name='envio_pedido'),
    path('<int:pk>/pedidos/', views.Pedidos, name='pedidos'),
    path('stor/', views.store, name="stor"),
    path('crear/', CrearNegocio.as_view(), name='crear_negocio'),
    path('editar/<int:pk>', NegocioUpdate.as_view(), name='editar_negocio'),
    path('producto/crear/<int:pk_negocio>/', CrearProducto.as_view(), name='crear_producto'),
    path('producto/editar/<int:pk>', ProductoUpdate.as_view(), name='editar_producto'),
    path('search/', views.search, name="search"),
    path('producto/<int:id>/list', ListProductos.as_view(), name='list_producto'),
    path('producto/delete', views.DeleteProducto, name='delete_producto'),
    path('calificacion-negocio/', views.calificacion_negocio, name='calificacion_negocio'),
    path('<int:pk>/comentarios/list/', CalificacionesNegocio.as_view(), name="cali_list"),
    path('comentario/delete/', DeleteComentarios, name='delatecoment')
], 'negocios')