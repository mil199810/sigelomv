from django.contrib import admin
from cal.models import Event, CategoriaServicio, Servicio, ScheduleServicio, Reservation, Queue, TiendaServicio, CalificacionTiendaServicio
from import_export.admin import ImportExportModelAdmin
from import_export import resources


class CategoriaServiciosAdmin(ImportExportModelAdmin):
    pass



# Register your models here.
admin.site.register(Event)
admin.site.register(Servicio)
admin.site.register(CategoriaServicio, CategoriaServiciosAdmin)
admin.site.register(ScheduleServicio)
admin.site.register(Reservation)
admin.site.register(Queue)
admin.site.register(TiendaServicio)
admin.site.register(CalificacionTiendaServicio)