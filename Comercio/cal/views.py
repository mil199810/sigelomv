import locale
import unicodedata
from datetime import datetime, timedelta, date,time
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import CreateView, ListView, UpdateView, View, DetailView
from django.urls import reverse, reverse_lazy
from django.utils.safestring import mark_safe
from django.db.models import Sum
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from django.contrib import messages
import calendar
from django.contrib.auth.decorators import login_required

from django.contrib.sites.shortcuts import get_current_site

from django.contrib.auth.models import User




from .models import Event, Servicio, ScheduleServicio, Reservation, Queue, TiendaServicio, CalificacionTiendaServicio
from profiles.models import Perfil

from .utils import Calendar
from .forms import EventForm, ServicioForm, EventConfirmationForm, TiendaServicioForm

@login_required
def calendar_view(request,pk, flag=None):    
    context = {}    
    pk_u = request.user.pk    
    if pk == str(pk_u):
         if flag == '1':
            context['flag'] = '1'   
    date = get_date(request.GET.get('month', None))    
    cal = Calendar(date.year, date.month)
    print(flag, type(flag))
    html_cal = cal.formatmonth(withyear=True,pk=pk,current_user=request.user.pk, flag=flag)
    context['servicios'] = Servicio.objects.filter(tienda__perfil__id_user=request.user.pk) ## para la modal que contiene el formualrio de reservacion
    context['calendar'] = mark_safe(html_cal)
    context['prev_month'] = prev_month(date)
    context['next_month'] = next_month(date)
    context['pk'] = pk    
    return render(request, 'cal/calendar.html', context)

def get_date(req_month):
    if req_month:
        year, month = (int(x) for x in req_month.split('-'))
        return date(year, month, day=1)
    return datetime.today()

def prev_month(d):
    first = d.replace(day=1)
    prev_month = first - timedelta(days=1)
    month = 'month=' + str(prev_month.year) + '-' + str(prev_month.month)
    return month

def next_month(d):
    days_in_month = calendar.monthrange(d.year, d.month)[1]
    last = d.replace(day=days_in_month)
    next_month = last + timedelta(days=1)
    month = 'month=' + str(next_month.year) + '-' + str(next_month.month)
    return month

def event(request, event_id=None):
    instance = Event()
    if event_id:
        instance = get_object_or_404(Event, pk=event_id)
    else:
        instance = Event()

    form = EventForm(request.POST or None, instance=instance)
    if request.POST and form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('cal:calendar'))
    return render(request, 'cal/event.html', {'form': form})


def ssl(request):
    response = render(request, '6F4A35B1D5D68CDA4270F888D0D82FDC.txt', content_type='text/plain')
    return response

def send_email(request, servicio, usuario, day, opcion):
    context = {}
    if opcion == 1:
        host = request.get_host() # se haya el nombre del host, para pasarse al contexto de la plantilla correo
        # se encunetra el id de reservacion, para crear el link de actualizacion
        reservacion = Reservation.objects.get(servicio=servicio, user=usuario, day=day)    
        id_reservation = reservacion.pk
        mail = reservacion.servicio.tienda.perfil.id_user.email
        site = get_current_site(request)        
        if reservacion:
            context['host'] = host
            context['reservation'] = id_reservation
            context['servicio'] = reservacion.servicio.nombre
            template = get_template('cal/correo.html')
            content = template.render(context)

            email = EmailMultiAlternatives(
                'Reservacion en SIGELO',
                'SIGELO APP',
                settings.EMAIL_HOST_USER,
                [mail]
            )
            email.attach_alternative(content, 'text/html')
            email.send()
            print('correo enviado-simulado')
            return 0
        print('no se encontro reservation.revisar consulta')
    elif opcion == 2:
        host = request.get_host()
        print(servicio.estado, 'andrei')
        if servicio.estado == True:
            context['servicio'] = servicio.servicio
            context['telefono'] = servicio.servicio.tienda.telefono
            context['day'] = servicio.day
            context['inicio'] = servicio.from_t
            context['fin'] = servicio.to_t
            template = get_template('cal/correo_usuario.html')
            content = template.render(context)

            email = EmailMultiAlternatives(
                'Reservacion en SIGELO',
                'SIGELO APP',
                settings.EMAIL_HOST_USER,
                [usuario]
            )
            email.attach_alternative(content, 'text/html')
            email.send()
            print('correo enviado-simulado-aceptado')
            return 0
        else:
            context['servicio'] = servicio.servicio
            context['telefono'] = servicio.servicio.tienda.telefono
            context['mensaje'] = 'https://api.whatsapp.com/send?phone=57'+servicio.servicio.tienda.telefono
            context['host'] = host
            context['pk'] = servicio.servicio.tienda.perfil.id_user.email
            context['day'] = servicio.day
            context['inicio'] = servicio.from_t
            context['fin'] = servicio.to_t
            template = get_template('cal/correo_usuario_cancelar.html')
            content = template.render(context)

            email = EmailMultiAlternatives(
                'Reservacion en SIGELO',
                'SIGELO APP',
                settings.EMAIL_HOST_USER,
                [usuario]
            )
            email.attach_alternative(content, 'text/html')
            email.send()
            print('correo enviado-simulado-rechazo')
            return 0



def DeleteServicio(request):
    pk = request.GET.get('pk')        
    servicio = Servicio.objects.get(id=pk)
    pk_tienda = servicio.tienda.id
    servicio.delete()
    return redirect('cal:mis-servicios', pk=pk_tienda)


        



@login_required
def service_schedule(request, year, month, slug, pk, pk_service, user_r=None):
    current_day = date(int(year), int(month), int(slug))
    #print('fecha', type(current_day), current_day.day, slug)
    locale.setlocale(locale.LC_TIME, 'es_CO.utf8')
    day = current_day.strftime("%A")
    for i in day:			
        if ord(i) == 169:
            day = 'miercoles'                
            break
        elif ord(i) == 161:
            day = 'sabado'
            break   
    day = elimina_tildes(day)
    day.lower()
    current_service = Servicio.objects.get(id=pk_service)
    slot = current_service.time
    dueno_service = pk
    print('pk',pk)
    print('request',request.user.pk)
    schedule = ScheduleServicio.objects.get(servicio=current_service, day=day)
    print(schedule, '09878')
    content=''
    local = 0
    if(schedule):        
        schedule_time1 = {}
        schedule_time2 = {}
        schedule_time1_full = {}
        schedule_time2_full = {}
        if(schedule.from_1):
            from_1 = schedule.from_1
            to_1 = schedule.to_1
            from_1_time = from_1.split(":")
            to_1_time = to_1.split(":")
            start_time_1 = time(int(from_1_time[0]), int(from_1_time[1]))
            end_time_1 = time(int(to_1_time[0]), int(to_1_time[1]))
            start_time_1 = datetime.strptime(from_1_time[0] + ":" + from_1_time[1], '%H:%M').time()
            end_time_1 = datetime.strptime(to_1_time[0] + ":" + to_1_time[1], '%H:%M').time()
            for a in list(time_slots(start_time_1, end_time_1, slot)):
                d = datetime.strptime(a, "%H:%M %p").time()
                a = d.strftime("%I:%M %p")
                if Reservation.objects.filter(servicio=current_service, day=current_day, from_t=a).exists():
                    number_of_reservations=Reservation.objects.filter(servicio=current_service, day=current_day, from_t=a).count()
                    queues= Queue.objects.filter(servicio=current_service)
                    maximum_reservation = queues.aggregate(Sum('people_per_time'))['people_per_time__sum']
                    if maximum_reservation == None:
                        maximum_reservation = 0
                    if(number_of_reservations>=maximum_reservation):
                        schedule_time1_full[a]=a
                schedule_time1[a] = a
        if (schedule.from_2):
            from_2 = schedule.from_2
            to_2 = schedule.to_2
            from_2_time = from_2.split(":")
            to_2_time = to_2.split(":")
            start_time_2 = datetime.strptime(from_2_time[0] + ":" + from_2_time[1], '%H:%M').time()
            end_time_2 = datetime.strptime(to_2_time[0] + ":" + to_2_time[1] , '%H:%M').time()
            for a in list(time_slots(start_time_2, end_time_2, slot)):
                d = datetime.strptime(a, "%H:%M %p").time()
                a = d.strftime("%I:%M %p")
                aa = d.strftime("%H:%M %p")                
                if Reservation.objects.filter(servicio=current_service, day=current_day, from_t=aa).exists():
                    print("Si hay para esa hora!")
                    number_of_reservations=Reservation.objects.filter(servicio=current_service, day=current_day, from_t=aa).count()
                    print("Number of reservations",number_of_reservations)
                    queues= Queue.objects.filter(servicio=current_service)
                    maximum_reservation = queues.aggregate(Sum('people_per_time'))['people_per_time__sum']
                    if maximum_reservation == None:
                        maximum_reservation = 0
                    if(number_of_reservations>=maximum_reservation):
                        schedule_time2_full[a]=a                    
                schedule_time2[a] = a
                #print('mil',schedule_time2)
    if request.method == "POST":
        # print('entra aqui')
        # print(current_service)
        # print(year)
        # print(month)
        # print(slug)
        date_time_str = request.POST.get("schedule")
        # print('miltoooasncjasndiniseincjisdncjisdn',date_time_str)
        from_schedule = datetime.strptime(date_time_str, '%I:%M %p').time()
        print('From:', from_schedule)
        to_schedule = (datetime.combine(date.today(), from_schedule) +
             timedelta(minutes=int(slot))).time()
        print('To:', to_schedule)
        # llega hasta aquí
        print(user_r)# no toma el none, en reservacion normal
        if user_r != None:
            user_r = User.objects.get(pk=user_r)
            if(Reservation.objects.filter(user=user_r, servicio=current_service, day=current_day).exists()):
                reservation = Reservation.objects.get(user=user_r,servicio=current_service, day=current_day)            
                reservation.from_t=from_schedule
                reservation.to_t=to_schedule
                reservation.save()
                print('enviando correo 1')
                messages.success(request, 'Se hizo un agendamiento exitoso!')
                send_email(request, servicio=reservation.servicio.id, usuario=user_r, day=current_day, opcion=1)#pasarle el dia actual tambien las horas de resrvacion variables = {from_schedule to_schedule}
                
            else:
                reservation = Reservation(user=user_r, servicio=current_service, day=current_day, from_t=from_schedule, to_t=to_schedule)            
                reservation.save()
                print('enviando correo 2')
                messages.success(request, 'Se realizó un agendamiento exitoso!')
                send_email(request, servicio=reservation.servicio.id, usuario=user_r, day=current_day, opcion=1)
        else:
            print('entra al segundo if', request.user.pk)
            print(from_schedule)
            if(Reservation.objects.filter(user=request.user, servicio=current_service, day=current_day).exists()):
                reservation = Reservation.objects.get(user=request.user,servicio=current_service, day=current_day)            
                reservation.from_t=from_schedule
                reservation.to_t=to_schedule
                reservation.save()
                print('enviando correo 3')
                messages.success(request, 'Se realizó un agendamiento exitoso!')
                send_email(request, servicio=reservation.servicio.id, usuario=request.user.pk, day=current_day, opcion=1)#pasarle el dia actual tambien las horas de resrvacion variables = {from_schedule to_schedule}                
            else:
                reservation = Reservation(user=request.user, servicio=current_service, day=current_day, from_t=from_schedule, to_t=to_schedule)            
                reservation.save()
                print('enviando correo 4')
                messages.success(request, 'Se realizó un agendamiento exitoso!')
                send_email(request, servicio=reservation.servicio.id, usuario=request.user.pk, day=current_day, opcion=1)            
        return redirect('cal:service_schedule', pk=pk, pk_service=pk_service,year=year, month=month, slug=slug)
    else:
        context = {
            'pk': pk,
            'year': year,
            'month': month,
            'day': slug,
            'dayname': day,
            'time1': schedule_time1,
            'time2': schedule_time2,
            'schedule_time1_full': schedule_time1_full,
            'schedule_time2_full': schedule_time2_full,
        }
        if user_r != None:
            context['user_r'] = user_r
        reservation = Reservation.objects.filter(user=request.user.pk, servicio=current_service, day=current_day)
        if(reservation.exists()):
            reservation=Reservation.objects.get(user=request.user, servicio=current_service, day=current_day)
            reservation_time = reservation.from_t.strftime("%I:%M %p")
            print("Nombres users", reservation.user.perfil.nombres)
            print("Telefono", reservation.user.perfil.telefono)
            print("Time")
            print(reservation_time)
            context['nombre']=reservation.user.perfil.nombres
            context['telefono']=reservation.user.perfil.telefono
            context['reservation']=reservation_time
            context['oferta']=int(dueno_service)
            print("Reservacion")
            print(context['reservation'])
        #messages.success(request, 'Se realizó un agendamiento exitoso!')
        return render(request, 'cal/agenda.html', context)

def time_slots(start_time, end_time, minutes):
    t = start_time
    while t <= end_time:
        yield t.strftime('%H:%M %p')
        t = (datetime.combine(date.today(), t) +
             timedelta(minutes=minutes)).time()

def ssl(request):
    filename = "6F4A35B1D5D68CDA4270F888D0D82FDC.txt"
    content = 'E5DCEC9E50B7E50FE244CC3702E9C3C04FF209DD4FE208087E90BF4A9E851A73\
               ssl.com\
               7ffd4c1e9a'
    response = render(request, 'B31859471C93FF9991984E7C0EF9F622.txt', content_type='text/plain')
    return response


@login_required
def home(request):
    current_user = request.user
    services = Servicio.objects.filter(tienda__perfil__id_user=current_user)
    week_days = ['lunes','martes','miercoles','jueves','viernes','sabado', 'domingo']
    schedule_list = {}
    schedule_time = {}
    for service in services:
        for day in week_days:
            schedule_filter = ScheduleServicio.objects.filter(user=current_user,servicio=service, day=day)
            if schedule_filter.exists():
                schedule_day = ScheduleServicio.objects.get(user=current_user,servicio=service, day=day)
                print(schedule_day)
                schedule_list[str(service.id) + '-' + day]= schedule_day.active
                schedule_time[str(service.id)+"-"+day+"-from_1"]= schedule_day.from_1
                schedule_time[str(service.id) + "-" + day + "-to_1"] = schedule_day.to_1
                schedule_time[str(service.id) + "-" + day + "-from_2"] = schedule_day.from_2
                schedule_time[str(service.id) + "-" + day + "-to_2"] = schedule_day.to_2
    contexto = {'servicios':services, 'schedule_list':schedule_list, 'schedule_time':schedule_time,'week_days':week_days}
    if request.method == "POST":
        for week_day in  week_days:
            lookup_day = 'actualizar-'+week_day
            if lookup_day in request.POST:
                for service in services:
                    lookup_service_day = str(service.id)+"-"+week_day
                    if lookup_service_day in request.POST:
                        from_1 = []
                        from_2 = []
                        to_1 = []
                        to_2 = []
                        if request.POST.get(str(service.id)+"-"+week_day+"-from_1"):
                            from_1 = request.POST.get(str(service.id)+"-"+week_day+"-from_1").split(" ")
                            to_1 = request.POST.get(str(service.id)+"-"+week_day+"-to_1").split(" ")
                        if request.POST.get(str(service.id)+"-"+week_day+"-from_2"):
                            from_2 = request.POST.get(str(service.id)+"-"+week_day+"-from_2").split(" ")
                            to_2 = request.POST.get(str(service.id)+"-"+week_day+"-to_2").split(" ")
                        if ScheduleServicio.objects.filter(user=request.user,servicio=service, day=week_day).exists():
                            schedule = ScheduleServicio.objects.get(user=request.user,servicio=service, day=week_day)
                            if len(from_1) > 1:
                                schedule.from_1 = from_1[0]
                                schedule.to_1 = to_1[0]                              
                            else:
                                schedule.from_1 = ''
                                schedule.to_1 = ''
                            if len(from_2) > 1:
                                schedule.from_2 = from_2[0]
                                schedule.to_2 = to_2[0]
                                
                            else:
                                schedule.from_2 = ''
                                schedule.to_2 = ''
                            schedule.active=True                            
                            schedule.save()
                        else:
                            new_schedule= ScheduleServicio(day=week_day,user=request.user,servicio=service)
                            if request.POST.get(str(service.id)+"-"+week_day+"-from_1"):
                                new_schedule.from_1 = from_1[0]
                                new_schedule.to_1 = to_1[0]
                            if request.POST.get(str(service.id)+"-"+week_day+"-from_2"):
                                new_schedule.from_2 = from_2[0]
                                new_schedule.to_2 = to_2[0]                    
                            new_schedule.save()
                    elif ScheduleServicio.objects.filter(user=request.user,servicio=service,day=week_day).exists():
                        schedule = ScheduleServicio.objects.get(user=request.user,servicio=service,day=week_day)
                        schedule.active =  False                        
                        schedule.save()
        return redirect('cal:listar_servicio') 
        print('Redirecionó')
    else:
        print('No post')
        return render(request, 'cal/listar_servicio.html', contexto)



class CrearServicio(CreateView):
    model = Servicio
    form_class = ServicioForm
    template_name = 'cal/crear_servicio.html'
    success_url = reverse_lazy('cal:tienda-list') 
    
    def form_valid(self, form):
        tienda = self.kwargs.get('pk_tienda', None)
        print(tienda)
        tienda = TiendaServicio.objects.get(id=tienda)
        form.instance.tienda = tienda
        return super().form_valid(form)


class ServicioUpdate(UpdateView):
    model = Servicio
    template_name = 'cal/update_servicio.html'
    form_class = ServicioForm
    success_url = reverse_lazy('cal:tienda-list')

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)



class ServicioDetail(DetailView):
    model = Servicio
    template_name = 'cal/detail_servicio.html'



class ServicioList(ListView):
    model = Servicio
    template_name = 'cal/list_servicio.html'    
    paginate_by = 6
    ordering = ['-updated']

    def get(self, request, *args, **kwargs):
        context = {}
        id= self.kwargs['pk']
        context['id'] = id                   
        queryset = Servicio.objects.filter(tienda__perfil__id_user=self.request.user.id, tienda__id=id)
        context['servicios'] = queryset
        return render(request, self.template_name, context=context)


class CrearTiendaServicio(CreateView):
    model = TiendaServicio
    form_class = TiendaServicioForm
    template_name = 'cal/tienda_form.html'
    success_url = reverse_lazy('cal:tienda-list')
    
    def form_valid(self, form):
        user = Perfil.objects.get(id=self.request.user.perfil.id)     
        form.instance.perfil = user
        return super().form_valid(form)


class TiendaServicioList(ListView):
    model = TiendaServicio
    template_name = 'cal/list_tienda.html'    
    paginate_by = 6
    ordering = ['-updated']


    def get_context_data(self, **kwargs):
        kwargs = {}
        tiendas = TiendaServicio.objects.filter(perfil__id_user=self.request.user.id)
        kwargs['tiendas'] = tiendas
        return kwargs


    def get_queryset(self):
        queryset = TiendaServicio.objects.filter(perfil__id_user=self.request.user.id)
        ordering = self.get_ordering()
        if ordering:
            if isinstance(ordering, str):
                ordering = (ordering,)
            queryset = queryset.order_by(*ordering)
        return queryset


class TiendaServicioUpdate(UpdateView):
    model = TiendaServicio
    template_name = 'cal/update_tienda.html'
    form_class = TiendaServicioForm
    success_url = reverse_lazy('cal:tienda-list')


def DeleteTiendaServicio(request):        
    pk = request.GET.get('pk')      
    tienda = TiendaServicio.objects.get(id=pk)    
    tienda.delete()
    return redirect('cal:tienda-list')


@login_required
def queues(request, pk, pk_service):
    current_user = request.user
    services = Servicio.objects.filter(tienda__perfil__id_user=pk, pk=pk_service)
    queues = Queue.objects.filter(servicio__in=services)
    context = {'services': services, 'queues': queues, 'pk': pk}
    if 'create_queue' in request.POST:
        if request.POST.get("id"):
                current_queue= Queue.objects.get(pk=request.POST.get("id"))
                if request.POST.get("name"):
                    current_queue.name = request.POST.get("name")
                    #service = request.POST.get("service_queue")
                    current_service = Servicio.objects.get(pk=pk_service)
                    current_queue.service = current_service
                    people_per_time = request.POST.get("people_per_time")
                    current_queue.people_per_time = people_per_time
                    current_queue.save()
                    return redirect('cal:queues', pk=pk, pk_service=pk_service)
        else:
            name = request.POST.get("name")
            #service = request.POST.get("service_queue")
            current_service = Servicio.objects.get(pk=pk_service)
            people_per_time = request.POST.get("people_per_time")
            new_queue = Queue(name=name, servicio=current_service, people_per_time=people_per_time)
            new_queue.save()
            return redirect('cal:queues', pk=pk, pk_service=pk_service)
    if 'delete_queue' in request.POST:
            id = int(request.POST.get("id"))
            current_queue = Queue.objects.get(pk=id)
            current_queue.delete()
            return redirect('cal:queues', pk=pk, pk_service=pk_service)
    else:
        return render(request, 'cal/queues.html', context)


class ListReservation(ListView):
    model = Reservation
    template_name = 'cal/list_reservation.html'
    paginate_by=8
    ordering=['-from_t']

    def get(self, request, *args, **kwargs):
        contexto = {}
        day=self.kwargs['day']
        print('day',type(day))
        reservations = Reservation.objects.filter(servicio=self.kwargs['pk_service'],day__day=day)
        contexto['reservations'] = reservations
        print(reservations)
        return render(request, self.template_name, context=contexto)


class ListMyReservation(ListView):
    model = Reservation
    template_name = 'cal/list_my_reservation.html'
    paginate_by=1
    ordering=['-from_t']

    def get(self, request, *args, **kwargs):
        contexto = {}
        day=self.kwargs['day']
        servicio = self.kwargs['pk_service']
        #print('day',type(day))
        reservations = Reservation.objects.filter(servicio=servicio,user=request.user,day__day=day)
        contexto['reservations'] = reservations
        #print(reservations)
        return render(request, self.template_name, context=contexto)




def ConfirmarLlegada(request):
    pk = request.GET.get('recipient-name', 1)
    print(pk, 'vamos')
    reservacion = Reservation.objects.get(pk=pk)

    if request.method == 'GET':
        estado = request.GET.get('estado', False)        
        if estado == '1':
            print('entra aquijbhhbhb')
            reservacion.confirmacion_llegada = True
            reservacion.save()                     
            return redirect('cal:calendar', pk=str(request.user.pk), flag='1')
        else:
            print('entra aqui -no llego')
            reservacion.confirmacion_llegada = False   
            reservacion.save()         
            return redirect('cal:calendar', pk=str(request.user.pk), flag='1')  




def ConfirmarReservacion(request, pk):
    contexto = {}    
    reservacion = Reservation.objects.get(pk=pk)
    if reservacion.servicio.tienda.perfil.id_user.pk != request.user.pk:
        return redirect('account_login')
    if request.method == 'GET':        
        contexto['pk'] = pk
        contexto['reservacion'] = reservacion
        #print(reservacion.estado, 'andrei')
        return render(request, 'cal/confirmar_reservacion.html', context=contexto)

    if request.method == 'POST':
        estado = request.POST.get('estado', False)        
        if estado == '1':
            reservacion.estado = True
            reservacion.pendiente = False
            print('cambio', reservacion.estado)
            user = reservacion.user.email            
            reservacion.save()
            print('Reservacion aceptada!!')
            send_email(request, servicio=reservacion, usuario=user, day=None, opcion=2)
            return redirect('core:home')
        else:
            reservacion.pendiente = False
            reservacion.estado = False
            user = reservacion.user.email            
            reservacion.save()            
            send_email(request, servicio=reservacion, usuario=user, day=None, opcion=2)
            print('Reservacion rechazada!!')
            return redirect('core:home')


def reservacion_modal(request):    
    pk = request.GET.get('recipient-name', 1)
    print(pk, 'vamos')
    reservacion = Reservation.objects.get(pk=pk)

    if request.method == 'GET':
        estado = request.GET.get('estado', False)        
        if estado == '1':
            reservacion.estado = True
            reservacion.pendiente = False
            print('cambio', reservacion.estado)
            user = reservacion.user.email            
            reservacion.save()
            print('Reservacion aceptada!!')
            send_email(request, servicio=reservacion, usuario=user, day=None, opcion=2)
            return redirect('cal:calendar', pk=str(request.user.pk), flag='1')
        else:
            print('entra aqui')
            reservacion.pendiente = False
            reservacion.estado = False
            user = reservacion.user.email            
            reservacion.save()            
            send_email(request, servicio=reservacion, usuario=user, day=None, opcion=2)
            print('Reservacion rechazada!!')
            return redirect('cal:calendar', pk=str(request.user.pk), flag='1')            



# funciona a medias 80%
def detallesServicio(request, pk):    
    context = {}
    tienda = TiendaServicio.objects.get(id=pk)
    context['tienda'] = tienda
    servicios = Servicio.objects.filter(tienda=tienda.id)
    #cantidad = CalificacionNegocio.objects.filter(negocio=negocio).count()
    #suma = CalificacionNegocio.objects.filter(negocio=negocio).aggregate(Sum('puntuacion'))
    context['servicios'] = servicios        
    context['pk'] = pk
    return render(request, 'cal/tienda_detalle.html', context=context)


def elimina_tildes(cadena):
    s = ''.join((c for c in unicodedata.normalize('NFD',cadena) if unicodedata.category(c) != 'Mn'))
    return s



def AddReservationForClient(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        email = request.POST.get('email-cliente')
        date = request.POST.get('date')
        servicios = request.POST.get('servicio')        
        d = datetime.strptime(date, "%Y-%m-%d")
        locale.setlocale(locale.LC_TIME, 'es_CO.utf8')                           
        date = d.strftime("%A") # cambio de fromato 
        year = d.strftime("%Y")
        month = d.strftime("%m")
        slug = d.strftime("%d")
        for i in date:
            if ord(i) == 169:
                date = 'Miercoles'                
                break
            elif ord(i) == 161:
                date = 'Sabado'
                break   
        date = elimina_tildes(date)
        date.lower()
        agenda = ScheduleServicio.objects.filter(servicio=servicios, user=request.user.pk, day=date, active=True).exists()
        pk = Servicio.objects.get(pk=servicios)        
        if agenda:
            if not User.objects.filter(username=username).exists() and not User.objects.filter(email=email).exists():
                    print(agenda, 'se puede agendar esta fecha', d)
                    user = User.objects.create_user(username=username, password=username, email=email)
                    user.save()
                    print(user.pk)
                    return redirect('cal:service_schedule', pk = str(pk.tienda.perfil.id_user.pk), pk_service=pk.pk, year=year, month=month, slug=slug, user_r=user.pk)
            else:
                if User.objects.filter(username=username).exists():
                    user = User.objects.get(username=username)
                else:
                    user = User.objects.get(email=email)
                return redirect('cal:service_schedule', pk = str(pk.tienda.perfil.id_user.pk), pk_service=pk.pk, year=year, month=month, slug=slug, user_r=user.pk)
        else:
            print('no hay agenda-----------------------------------------------------------------------')
            messages.success(request, 'No hay agenda disponible para esta fecha ', date)
            return redirect('cal:calendar', pk=request.user.pk, flag=1)


"""Calificacion de tienda de servicios"""
def calificacion_negocio(request):
    puntos = request.GET.get('estrellas', 1)
    id_tienda = request.GET.get('id_tienda')
    comentario = request.GET.get('comentario')
    print(puntos, id_tienda, request.user.id, comentario)
    CalificacionTiendaServicio.objects.create(puntuacion=puntos, tienda_id=id_tienda, user=request.user, comentario=comentario)
    return redirect('core:home')



class CalificacionesTienda(ListView):
    model = CalificacionTiendaServicio
    template_name = 'cal/list_calificaciones.html'
    paginate_by = 9
    ordering = ['-created']


    def get_queryset(self):
        user = self.kwargs.get('pk_tienda', None)
        print(user)
        queryset = CalificacionTiendaServicio.objects.filter(tienda__perfil__id_user=user)
        ordering = self.get_ordering()
        if ordering:
            if isinstance(ordering, str):
                ordering = (ordering,)
            queryset = queryset.order_by(*ordering)
        print(queryset)
        return queryset


def DeleteComentarios(request):
    url_anterior = request.META.get('HTTP_REFERER')
    pk = request.GET.get('option', '')
    if pk != '':
        c = CalificacionTiendaServicio.objects.get(pk=pk)
        c.delete()
        print('eliminada')
        return redirect(url_anterior)
    else:
        return redirect(url_anterior)


def service_agenda(request, pk, pk_service):
    week_days = ['lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado', 'domingo']
    schedule_list = {}
    schedule_time = {}
    current_service = Servicio.objects.get(tienda__perfil__id_user=pk, pk=pk_service)

    for day in week_days:
        schedule_filter = ScheduleServicio.objects.filter(user=request.user, servicio=current_service, day=day)
        if schedule_filter.exists():
            schedule_day = ScheduleServicio.objects.get(user=request.user, servicio=current_service, day=day)
            schedule_list[day] = schedule_day.active
            schedule_time[day + "-from_1"] = schedule_day.from_1
            schedule_time[day + "-to_1"] = schedule_day.to_1
            schedule_time[day + "-from_2"] = schedule_day.from_2
            schedule_time[day + "-to_2"] = schedule_day.to_2
    if request.POST:
        for week_day in week_days:
            if request.POST.get("actualizar-" + week_day):
                from_1 = 0
                to_1 = 0
                from_2 = 0
                to_2 = 0
                if request.POST.get(week_day + "-from_1"):
                    from_1 = request.POST.get(week_day + "-from_1")
                if request.POST.get(week_day + "-to_1"):
                    to_1 = request.POST.get(week_day + "-to_1")
                if request.POST.get(week_day + "-from_2"):
                    from_2 = request.POST.get(week_day + "-from_2")
                if request.POST.get(week_day + "-to_2"):
                    to_2 = request.POST.get(week_day + "-to_2")
                if ScheduleServicio.objects.filter(user=request.user, servicio=current_service, day=week_day).exists():
                    schedule = ScheduleServicio.objects.get(user=request.user, servicio=current_service, day=week_day)
                    if from_1 != 0:
                        schedule.from_1 = from_1
                    if to_1 != 0:
                        schedule.to_1 = to_1
                    if from_2 != 0:
                        schedule.from_2 = from_2
                    if to_2 != 0:
                        schedule.to_2 = to_2
                    if request.POST.get("checked-" + week_day):
                        schedule.active=True
                    else:
                        schedule.active=False
                    schedule.save()
                else:
                    schedule = ScheduleServicio(user=request.user, servicio=current_service,day=week_day)
                    if from_1 != 0:
                        schedule.from_1 = from_1
                    if to_1 != 0:
                        schedule.to_1 = to_1
                    if from_2 != 0:
                        schedule.from_2 = from_2
                    if to_2 != 0:
                        schedule.to_2 = to_2
                    if request.POST.get("checked-" + week_day):
                        schedule.active=True
                    else:
                        schedule.active=False  
                    schedule.save()
                return redirect("cal:mis-servicios", pk=current_service.tienda.id)


    context = {'pk_service': pk_service, 'service':current_service, 'schedule_list': schedule_list,
               'schedule_time': schedule_time, 'week_days': week_days}
    return render(request, 'cal/service_agenda.html', context)
