from django import template

register = template.Library()

@register.filter(name='lookup')
def lookup(value, arg):
    if arg in value:
        return value[arg]
    else:
        return 0

@register.filter(name='lookup_admin')
def lookup(dic, company):
    for user_company in dic:
        if user_company.company == company:
            return user_company.user
    return None