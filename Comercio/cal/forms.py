from django import forms
from django.forms import ModelForm, DateInput
from cal.models import Event, Servicio, ScheduleServicio, TiendaServicio


class TiendaServicioForm(forms.ModelForm):
  class Meta:
    model = TiendaServicio
    fields = ('nombre', 'fachada', 'direccion',
              'telefono', 'email_negocio', 'lon', 'lat')
    label = {
        'nombre': 'Servicio',
        'fachada': 'Foto del Establecimiento',        
        'direccion': 'Dirección',        
        'telefono': 'Telefono del Establecimiento',        
        'email_negocio': 'Email del Establecimiento',
        'lon': '',
        'lat': '',     
    }

    widgets = {
        'nombre': forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Nombre del Servicio'
            }
        ),
        'fachada': forms.ClearableFileInput(
                attrs={
                    'class': 'form-control-file mt-3'
                }
        ),
        'direccion': forms.TextInput(
            attrs={
                'class': 'form-control',                
            }
        ),                        
        'telefono': forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Donde te van a contactar'
            }
        ),                       
        'email_negocio': forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Correo Electrónico del Establecimiento'
            }
        ),
        'lon': forms.NumberInput(
                attrs={
                    'hidden': True,
                    'required': True
                }
            ),
        'lat': forms.NumberInput(
                attrs={
                    'hidden': True,
                    'required': True
                }
        )
    }








class ScheduleServicioForm(forms.ModelForm):
    class Meta:
        model = ScheduleServicio
        fields = (
            'day',
            'active',
            'from_1',
            'to_1',
            'from_2',
            'to_2',
        )


class ServicioForm(forms.ModelForm):
  class Meta:
    model = Servicio
    fields = ('nombre', 'modalidad', 'foto_servicio', 'precio', 'descripcion',
              'categoria', 'time')
    label = {
        'nombre': 'Servicio',
        'modalidad': 'Modalidad del servicio',   
        'precio': 'Precio del Artículo', 
        'descripcion': 'Descripción',
        'foto_servicio': 'Foto del Establecimiento o servicio',        
        'categoria': 'Categoría del servicio',        
        'time': 'Franja de tiempo',        
    }

    widgets = {
        'nombre': forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Nombre del servicio'
            }
        ),
        'foto_servicio': forms.ClearableFileInput(
                attrs={
                    'class': 'form-control-file mt-3'
                }
        ),
        'modalidad': forms.Select(
            attrs={
                'class': 'form-control'
            }
        ), 
        'precio': forms.NumberInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Precio del servicio'
            }
        ),   
        'descripcion': forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Descripción'
            }
        ),                        
        'categoria': forms.Select(
            attrs={
                'class': 'form-control'
            }
        ),                       
        'time': forms.NumberInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Tiempo en Minutos'
            }
        )
    }


class EventForm(ModelForm):
  class Meta:
    model = Event
    # datetime-local is a HTML5 input type, format to make date time show on fields    
    widgets = {
        'start_time': DateInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
        'end_time': DateInput(attrs={'type': 'datetime-local', 'hidden': True}, format='%Y-%m-%dT%H:%M'),
    }
    fields = (
        'title', 'description', 'start_time', 'end_time'
    )

  def __init__(self, *args, **kwargs):
    super(EventForm, self).__init__(*args, **kwargs)
    # input_formats parses HTML5 datetime-local input to datetime field
    self.fields['start_time'].input_formats = ('%Y-%m-%dT%H:%M',)
    self.fields['end_time'].input_formats = ('%Y-%m-%dT%H:%M',)


estado = [
    ('True', 'Aceptar'),
    ('False', 'No aceptar')
    
]

class EventConfirmationForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = (
            'estado',
        )
        widgets = {
            'estado': forms.Select(
                attrs={
                    'class': 'form-control'
                },
                choices=estado
            )        
        }
