from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from profiles.models import Perfil


grado_teletrabajo = [
    ('LOCAL', 'Mi local'),
    ('DOMICILIO', 'Domicilio'),
    ('DEFINIR', 'A Definir')
]



class TiendaServicio(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField('Nombre del Establecimiento', max_length=50)
    fachada = models.ImageField(
        'Fachada del Establecimiento', upload_to='fachadas/%Y/%m/%d/')
    direccion = models.CharField('Dirección', max_length=50)
    telefono = models.CharField('Número Telefónico', max_length=10)
    email_negocio = models.EmailField('Correo Electrónico del Negocio', blank=True)
    perfil = models.ForeignKey(Perfil, on_delete=models.CASCADE, verbose_name='Usuario asociado')
    lon = models.FloatField('')
    lat = models.FloatField('')
    created = models.DateTimeField(auto_now_add=True, blank=True, null=True , verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, blank=True, null=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = 'Tienda de servicios'
        ordering = ['-updated']
        verbose_name_plural = 'Tiendas de servicios'
        unique_together = (('nombre', 'direccion'),)


    def __str__(self):
        return self.nombre
    


class CategoriaServicio(models.Model):
    nombre = models.CharField('Nombre del servicio', max_length=75)

    class Meta:
        verbose_name = 'Categoría del servicio'
        verbose_name_plural = 'Categoría de los servicios'

    def __str__(self):
        return self.nombre


class Servicio(models.Model):
    id = models.AutoField(primary_key=True)
    tienda = models.ForeignKey(TiendaServicio, on_delete=models.CASCADE)
    nombre = models.CharField('Servicio', max_length=100)    
    foto_servicio = models.ImageField('Foto del Establecimiento', upload_to='servicios/locales/%Y/%m/%d/')    
    modalidad = models.CharField('Modalidad de Trabajo', max_length=50,
                                 choices=grado_teletrabajo, default=grado_teletrabajo[0])  
    precio = models.IntegerField('precio', default=0, null=True)    
    descripcion = models.TextField('Descripción', null=True) 
    categoria = models.ForeignKey(CategoriaServicio, on_delete=models.CASCADE)
    time = models.IntegerField('Tiempo de Servicio')
    created = models.DateTimeField(auto_now_add=True, blank=True, null=True , verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, blank=True, null=True, verbose_name="Fecha de edición")
      
    class Meta:
        verbose_name = 'Servicio'
        ordering = ['-updated']
        verbose_name_plural = 'Servicios'

    def __str__(self):
        return self.nombre

class ScheduleServicio(models.Model):
    day = models.CharField(max_length=15)
    active = models.BooleanField(default=True)
    from_1 = models.CharField(max_length=5)# hora inicial
    to_1 = models.CharField(max_length=5)
    from_2 = models.CharField(max_length=5)
    to_2 = models.CharField(max_length=5)# hora final
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
     

    def __str__(self):
        return self.day
    

class Event(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    start_time = models.DateTimeField()
    end_time = models.DateTimeField('', blank=True, null=True)
    estado = models.BooleanField('Estado', default=False)
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    @property
    def get_html_url(self):
        """Esta funcion retorna la url del modelo
        y la @property sirve para poderacceder a la funcion como un parametro
        normal"""
        url = reverse('cal:event_edit', args=(self.id,))
        return f'<a href="{url}"> {self.title} </a>'

    def __str__(self):
        return self.title

class Reservation(models.Model):
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    day = models.DateField()
    from_t = models.TimeField()
    to_t = models.TimeField()
    estado = models.BooleanField('estado',default=False)
    confirmacion_llegada = models.BooleanField('Confirmacion llegada', default=False)
    pendiente = models.BooleanField('pendiente', default=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, blank=True, null=True , verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, blank=True, null=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = 'Reservaciones'
        verbose_name_plural = 'Reservaciones'
        ordering = ['-updated']

    def __str__(self):
        return self.servicio.nombre
    
class Queue(models.Model):
    name = models.CharField(max_length=200)
    people_per_time = models.IntegerField()
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)

    def __str__(self):
        return self.name




class CalificacionTiendaServicio(models.Model):
    id = models.AutoField('id', primary_key=True)
    comentario = models.TextField('Comentario', blank=True)
    puntuacion = models.IntegerField('Puntuacion')    
    tienda = models.ForeignKey(TiendaServicio, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")


    class Meta:
        verbose_name = 'Calificacion'
        ordering = ['-created', 'puntuacion']
        verbose_name_plural = 'Calificaciones'

    def __str__(self):
        return str(self.puntuacion)



