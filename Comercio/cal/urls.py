from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls import url
from django.urls import path, include
from . import views
from .views import CrearServicio, ServicioUpdate, ServicioDetail, calendar_view, service_schedule, ListReservation,ListMyReservation
from .views import ServicioList, CrearTiendaServicio, TiendaServicioList, TiendaServicioUpdate, DeleteTiendaServicio, CalificacionesTienda, DeleteComentarios


app_name = 'cal'
urlpatterns = [
    path('<str:pk>/calendar/', views.calendar_view, name='calendar'),
    path('<str:pk>/calendar/<str:flag>/', views.calendar_view, name='calendar'),
    path('<str:pk>/queues/<int:pk_service>/', views.queues, name='queues'),
    path('<str:pk>/calendar/<str:flag>/<int:pk_service>/reservaciones/<int:day>/',ListReservation.as_view(), name='listar_reservaciones'),
    path('<str:pk>/calendar/<int:pk_service>/reservaciones/<int:day>/',ListReservation.as_view(), name='listar_reservaciones'),
    path('<str:pk>/calendar/<str:flag>/<int:pk_service>/misreservaciones/<int:day>/',ListMyReservation.as_view(), name='listar_mis_reservaciones'),
    path('<str:pk>/calendar/<int:pk_service>/<int:year>/<int:month>/<int:slug>/', views.service_schedule, name='service_schedule'),
    path('<str:pk>/calendar/<int:pk_service>/<int:year>/<int:month>/<int:slug>/<int:user_r>/', views.service_schedule, name='service_schedule'),
    url(r'^event/new/$', views.event, name='event_new'),
    url(r'^event/edit/(?P<event_id>\d+)/$', views.event, name='event_edit'),
    path('', views.home, name='listar_servicio'),    
    path('<int:pk_tienda>/crear/', CrearServicio.as_view(), name='crear_servicio'),
    path('mis-servicio/<int:pk>', ServicioList.as_view(), name='mis-servicios'),
    path('delete/', views.DeleteServicio, name='delete_servicio'),
    path('editar/<int:pk>', ServicioUpdate.as_view(), name='editar_servicio'),
    path('detail/<int:pk>', ServicioDetail.as_view(), name='detail_servicio'),
    path('detalles/<int:pk>/', views.detallesServicio, name="detalles_Servicio"),
    ## urls de reservacion
    path('reservacion/confirmar/<int:pk>', views.ConfirmarReservacion , name='confirmar_reservacion'),
    path('reservacion/modal/estado', views.reservacion_modal, name='confirmarmodal'),
    path('crear/reservacion/for-client', views.AddReservationForClient, name='add-reservacion-client'),
    path('reservacion/llegada', views.ConfirmarLlegada, name='confirmacion-llegada'),
    # urls Tienda servicios
    path('tiendas/crear', CrearTiendaServicio.as_view(), name='crear-tienda'),
    path('me/tienda-servicios', TiendaServicioList.as_view(), name='tienda-list'),
    path('tienda/<int:pk>/', TiendaServicioUpdate.as_view(), name='tienda-update'),
    path('tienda/eliminar', DeleteTiendaServicio, name="tienda-delete"),    
    path('calificar-tienda/', views.calificacion_negocio, name='tienda-calificacion'),
    path('.well-known/pki-validation/B31859471C93FF9991984E7C0EF9F622.txt', views.ssl, name="ssl"),
    path('<int:pk_tienda>/comentarios/list/', CalificacionesTienda.as_view(), name="tienda-cali-list"),
    path('.well-known/pki-validation/284F712840C098E3B1903A9674547FA8.txt', views.ssl, name="ssl"),
    path('<str:pk>/service_agenda/<str:pk_service>/', views.service_agenda, name='service_agenda'),
    path('comentario/delete/', DeleteComentarios, name='delatecoment')
]
