from datetime import datetime, timedelta, date
from calendar import HTMLCalendar
from .models import Event, ScheduleServicio, Servicio, Reservation, Queue
import calendar
import locale
import unicodedata

def elimina_tildes(cadena):
    s = ''.join((c for c in unicodedata.normalize('NFD',cadena) if unicodedata.category(c) != 'Mn'))
    return s


class Calendar(calendar.LocaleHTMLCalendar):
	def __init__(self, year=None, month=None,*args):
		self.year = year
		self.month = month
		calendar.LocaleHTMLCalendar.__init__(self, locale='es_CO.utf8')
	def formatday(self, year, month, day, events, pk, current_user, flag):
		print(flag, 'formamonth', pk, 'pk', 'vrsion 3')		
		if day != 0:
			#print(year, month,day)
			today = date.today()
			d = date(int(year), int(month), int(day))
			locale.setlocale(locale.LC_TIME, 'es_CO.utf8')
			dayname = d.strftime("%A")
			#print(dayname) # aqui esta mal
			for i in dayname:			
				if ord(i) == 169:
					dayname = 'miercoles'                
					break
				elif ord(i) == 161:
					dayname = 'sabado'
					break   
			dayname = elimina_tildes(dayname)
			dayname.lower()
			#print(dayname, 'dia de la semana') # aqui sale bien
			#Esto lo va ver el dueño del servicio			
			services = Servicio.objects.filter(tienda__perfil__id_user=pk)		
			reservas = Reservation.objects.filter(user=pk,day=d)			
			#print(type(pk))
			#print(type(current_user))			
			schedule_services = ''
			schedule_reservas = ''

			if int(pk) == current_user:
				if flag == '0':
					print('entro a flag===0')					
					schedule_reservas = '<div class="list-group-item-secondary">'
					for reserva in reservas:
						print('reserva entro')					
						if(Reservation.objects.filter(user=pk).exists()):
							if(d>=today):
								if reserva.estado == False and reserva.pendiente == True:
									estado = 'Pendiente'
									schedule_reservas  += '<a  href="'+str(reserva.servicio.id)+'/misreservaciones'+'/'+str(day)+'" class="list-group-item list-group-item-info bg-danger">'+reserva.servicio.nombre +' : '+estado +'</a>'
								if reserva.estado == True:
									estado = 'Aceptada'
									schedule_reservas  += '<a  href="'+str(reserva.servicio.id)+'/misreservaciones'+'/'+str(day)+'" class="list-group-item list-group-item-info bg-info">'+reserva.servicio.nombre +' : '+estado +'</a>'
								if reserva.estado == False and reserva.pendiente == False: 
									estado = 'Rechazada'
									schedule_reservas  += '<a  href="'+str(reserva.servicio.id)+'/misreservaciones'+'/'+str(day)+'" class="list-group-item list-group-item-info bg-danger">'+reserva.servicio.nombre +' : '+estado +'</a>'
								print('entra aqui')
					schedule_reservas  += '</div><br>'
				else: 
					schedule_services = '<div class="list-group-item-secondary">'
					for service in services:
						if(ScheduleServicio.objects.filter(user=pk, servicio=service, day=dayname).exists()):
							schedule = ScheduleServicio.objects.get(user=pk, servicio=service, day=dayname)
							if(d>=today and schedule.active == True and Queue.objects.filter(servicio=service).exists() == True):
								schedule_services += '<a href="'+str(service.id)+'/reservaciones'+'/'+str(day)+'" class="list-group-item list-group-item-secondary bg-secondary">'+service.nombre+'</a>'
								#schedule_services += '<hr>'
							if(d>=today and (schedule.active == False or Queue.objects.filter(servicio=service).exists() == False)):
								schedule_services += '<a href="#" class="badge badge-danger">'+service.nombre+': No activo o no tiene fila</a>'
					schedule_services += '</div> <br>'
				return f"<td><span class='date'>{day}</span><ul> {schedule_services} {schedule_reservas}</ul></td>"
			else:
				print('Forastero')
				schedule_services = '<div class="list-group-item-secondary">'
				for service in services:
					#schedule = ScheduleServicio.objects.get(user=pk, servicio=service, day=dayname)
					#schedule.active
					if(ScheduleServicio.objects.filter(user=pk, servicio=service, day=dayname).exists()):
						schedule = ScheduleServicio.objects.get(user=pk, servicio=service, day=dayname)
						if(d>=today and schedule.active == True and Queue.objects.filter(servicio=service).exists()):
							schedule_services += '<a href="'+str(service.id)+'/'+str(year)+'/'+str(month)+'/'+str(day)+'" class="list-group-item list-group-item-primary bg-primary">'+service.nombre+'</a>'
						#if (d>=today and Queue.objects.filter(servicio=service).exists() == False):
						#	schedule_services += '<a href="#" class="badge badge-danger">Este servicio aún no tiene fila</a>'	
					schedule_services += '</div>'
				return f"<td><span class='date'>{day}</span><ul> {schedule_services} </ul></td>"					
		else:
			return '<td></td>'

	# formats a week as a tr
	def formatweek(self, year, month, theweek, events, pk,current_user, flag):
		print(flag, 'formamonth', pk, 'pk', 'vrsion 2')
		week = ''
		for d, weekday in theweek:
			week += self.formatday(year, month, d, events, pk,current_user, flag)
		return f'<tr> {week} </tr>'

	# formats a month as a table
	# filter events by year and month
	def formatmonth(self, withyear=True, pk=None, current_user=None, flag=None):
		print(flag, 'formamonth', pk, 'pk')
		events = Event.objects.filter(start_time__year=self.year, start_time__month=self.month)
		cal = f'<table border="0" cellpadding="0" cellspacing="0" class="calendar">\n'
		cal += f'{self.formatmonthname(self.year, self.month, withyear=withyear)}\n'
		cal += f'{self.formatweekheader()}\n'
		for week in self.monthdays2calendar(self.year, self.month):
			cal += f'{self.formatweek(self.year, self.month, week, events, pk,current_user, flag)}\n'
		cal += f'</table>'
		return cal
