from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from core.urls import urlpatterns
from profiles.urls import registro_perfil
from negocios.urls import registro_negocios
from django.views.static import serve

# if settings.DEBUG:
#     path_server = 'ingi/obj3/'
# else:
#     path_server = ''
path_server = 'servicios/'
"""
urlpatterns = [
    path('', include(urlpatterns)),
    path(path_server + 'admin/', admin.site.urls),
    path(path_server + 'accounts/', include('allauth.urls')),
    path(path_server + 'me/', include(registro_perfil)),
    path(path_server + 'negocios/', include(registro_negocios)),
    path(path_server + '', include(urlpatterns)),
    path(path_server + 'servicios/', include('cal.urls')),
    #path('', include('cal.urls')),
    #path('servicios/', include(registro_negocios)),
]
"""


urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path('me/', include(registro_perfil)),
    path('negocios/', include(registro_negocios)),
    path('', include(urlpatterns)),
   # path('.well-known/pki-validation/6F4A35B1D5D68CDA4270F888D0D82FDC.txt', include(urlpatterns)),
    path('servicios/', include('cal.urls')),
    #path('servicios/', include(registro_negocios)),    
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


#if settings.DEBUG:
#    from django.conf.urls.static import static
#    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
