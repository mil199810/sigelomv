FROM ubuntu:18.04

ENV TZ=America/Bogota
ENV PYTHONIOENCODING=utf-8
ENV OS_LOCALE="es_CO.utf8"
#ENV PYTHONUNBUFFERED 1
RUN ln -snf \
/usr/share/zoneinfo/$TZ \
/etc/localtime && echo $TZ > \
/etc/timezone
RUN apt-get update && apt-get install -y \
    wget \
    git \
    nano \
    sqlite3 \
    python3.7 \
    python3-pip \
    idle3 \  
 # python-dev \
    ssh \
   # python-boto3 \
    locales --no-install-recommends apt-utils \
    && pip3 install -U pip setuptools \
    && pip3 install --upgrade pip \
    && mkdir /code


WORKDIR /code
COPY . /code/
RUN pip install -r requirements.txt
#COPY ./allauth /usr/local/lib/python3.6/dist-packages/allauth/account/
CMD ["gunicorn", "-c", "config/gunicorn/conf.py", "--bind", ":8080", "--chdir", "Comercio", "Comercio.wsgi:application"]
